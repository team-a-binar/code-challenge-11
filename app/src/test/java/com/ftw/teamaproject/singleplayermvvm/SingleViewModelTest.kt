package com.example.cc10_single_mainmenu.singleplayermvvm

import androidx.lifecycle.Observer
import com.example.cc9_single_mainmenu.singleplayermvvm.Repository
import com.example.cc9_single_mainmenu.singleplayermvvm.SingleViewModel
import com.ftw.teamaproject.SinglePlayer.model.MatchesDummy
import com.ftw.teamaproject.helper.InstantRuleExecution
import com.ftw.teamaproject.helper.TrampolineSchedulerRX
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class SingleViewModelTest : Spek({
    Feature("Cek Hasil") {
        val repoSingle = mock<Repository>()
        val viewModel = SingleViewModel(repoSingle)
        val observeState = mock<Observer<MatchesDummy>>()

        beforeFeature {
            InstantRuleExecution.start() //set bg process
            TrampolineSchedulerRX.start()

            viewModel.updateMatchState.observeForever(observeState)
        }

        Scenario("Ngambil data kalau player yang menang") {
            Given("Hasil main") {

                val dummy = mock<MatchesDummy>()
                given(repoSingle.updateMatches("Batu", "Gunting", "Player")).willReturn(
                    Single.just(
                        dummy
                    )
                )
            }

            When("Munculnya player yang menang") {
                viewModel.updateMatch("Batu", "Gunting", "Player")
            }

            Then("Hasil main player menang muncul") {
                viewModel.updateMatchState.test().assertHasValue()
            }
        }

        afterFeature {
            InstantRuleExecution.tearDown()
            TrampolineSchedulerRX.tearDown()
        }
    }
}
)
package com.ftw.teamaproject.multiPlayerMenu

import androidx.lifecycle.Observer
import com.ftw.teamaproject.helper.InstantRuleExecution
import com.ftw.teamaproject.helper.TrampolineSchedulerRX
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.model.User
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class MultiPlayerMenuViewModelTest : Spek({

    Feature("Get Data") {
        val repository = mock<RepositoryMultiPlayerMenu>()
        val viewModel = MultiPlayerMenuViewModel(repository)
        val obs = mock<Observer<User>>()
        val obsMatch = mock<Observer<Matches>>()

        beforeFeature {
            InstantRuleExecution.start()
            TrampolineSchedulerRX.start()
            viewModel.mutableDataFriend.observeForever(obs)
            viewModel.mutableDataMatch.observeForever(obsMatch)
        }

        Scenario("GetDataFriend Success") {

            val mutable = mutableListOf("id")
            val mock = mock<User>()
            val user = User("id", "nama", "", "img", "", "", mutable, mutable, "", 0, false)

            Given("set value") {
                given(repository.getUsers("24")).willReturn(Single.just(user))
                given(repository.getFriendsData("id")).willReturn(Single.just(mock))
            }

            When("GetFriend Data") {
                viewModel.getFriend("24")

            }

            Then("result Ada") {
                verify(repository).getFriendsData("id")
                viewModel.mutableDataFriend.test().assertHasValue()
            }
        }

        Scenario("Get Data Match Success bisa") {
            val mutable = mutableListOf("id3")
            val user = User("id","nama","","img","","",mutable, mutable,"",0)
            val mockTimestamp = mock<Timestamp>()
            val dummyMatch =
                Matches("id3", "", "", "", "", "", "", "", "", mockTimestamp)



            Given("Set Data") {
                given(repository.getUsers("1")).willReturn(Single.just(user))
                given(repository.getMatch("id3")).willReturn(Single.just(dummyMatch))
            }

            When("GetMatchData") {
                viewModel.getMatch("1")
            }

            Then("hasil ada") {
                verify(repository).getUsers("1")
                viewModel.mutableDataMatch.test().assertHasValue()
            }
        }


        afterFeature {
            InstantRuleExecution.tearDown()
            TrampolineSchedulerRX.tearDown()
        }
    }
})
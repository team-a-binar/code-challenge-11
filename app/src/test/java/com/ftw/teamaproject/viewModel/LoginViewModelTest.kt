package com.ftw.teamaproject.viewModel

import androidx.lifecycle.Observer
import com.ftw.teamaproject.helper.InstantRuleExecution
import com.ftw.teamaproject.helper.TrampolineSchedulerRX
import com.ftw.teamaproject.ui.login.LoginRepository
import com.ftw.teamaproject.ui.login.LoginViewModel
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class LoginViewModelTest : Spek({
    Feature("Login"){
        val repository = mock<LoginRepository>()
        val viewModel = LoginViewModel(repository)
        val observeStateLoginTest = mock<Observer<String>>()

        beforeFeature {
            InstantRuleExecution.start()
            TrampolineSchedulerRX.start()

            viewModel.loginState.observeForever(observeStateLoginTest)
        }

        Scenario("login sukses"){
            Given("set email dan password"){
                given(repository.loginTest("abcd@email.com", "abcd")).willReturn(Single.just("abcde"))
            }

            When("Login"){
                viewModel.loginTest("abcd@email.com", "abcd")
            }

            Then("result email dan password not null"){
                viewModel.loginState.test().assertHasValue()
            }
        }

        afterFeature {
            InstantRuleExecution.tearDown()
            TrampolineSchedulerRX.tearDown()
        }

    }
})
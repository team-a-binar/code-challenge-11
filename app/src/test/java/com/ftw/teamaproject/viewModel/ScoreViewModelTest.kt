package com.ftw.teamaproject.viewModel

import com.ftw.teamaproject.ui.score.ScoreRepository
import com.ftw.teamaproject.ui.score.ScoreViewModel
import com.ftw.teamaproject.helper.InstantRuleExecution
import com.ftw.teamaproject.helper.TrampolineSchedulerRX
import com.ftw.teamaproject.model.User
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class ScoreViewModelTest: Spek({

    Feature("Score"){
        val repository = mock<ScoreRepository>()
        val viewModel = ScoreViewModel(repository)

        beforeFeature {
            InstantRuleExecution.start()
            TrampolineSchedulerRX.start()
        }

        Scenario("score list"){

            val mockList = mock<MutableList<User>>()
            Given("tampil score") {
                given(repository.getListUser()).willReturn(Single.just(mockList))
            }

            When("checked list"){
                viewModel.getListData()
            }

            Then("list sukses"){
                viewModel.userData.test().assertHasValue()
            }
        }

        afterFeature {
            InstantRuleExecution.tearDown()
            TrampolineSchedulerRX.tearDown()
        }
    }
})


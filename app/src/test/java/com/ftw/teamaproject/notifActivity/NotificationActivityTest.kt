package com.ftw.teamaproject.notifActivity

import com.ftw.teamaproject.helper.InstantRuleExecution
import com.ftw.teamaproject.helper.TrampolineSchedulerRX
import com.google.firebase.firestore.Query
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class NotificationActivityTest : Spek({
    Feature("NotificationActivity") {
        val repository = mock<NotificationRepository>()
        val viewModel = NotificationViewModel(repository)

        beforeFeature {
            InstantRuleExecution.start()
            TrampolineSchedulerRX.start()
        }

        Scenario("Skenario ambil data notifikasi") {
            Given("set value") {
                val dummy = mock<Query>()
                given(repository.getNotificationData("")).willReturn(Single.just(dummy))
            }

            When("logic") {
                viewModel.getNotificationData("")
            }

            Then("Data notifikasi ada") {
                viewModel.dataNotification.test().assertHasValue()
            }
        }
    }
})
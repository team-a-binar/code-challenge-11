package com.ftw.teamaproject.resultGame

import com.ftw.teamaproject.helper.InstantRuleExecution
import com.ftw.teamaproject.helper.TrampolineSchedulerRX
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.resultGame.ViewModel.ResultRepository
import com.ftw.teamaproject.resultGame.ViewModel.ResultViewModel
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class ResultActivityTest : Spek({

    Feature("ResultGame") {
        val repository = mock<ResultRepository>()
        val viewModel = ResultViewModel(repository)
        val dummyMatches = mock<Matches>()
        val dummyUser = mock<User>()
        val dummyMatch = Matches("ID","IDP1","IDP2","P1UNAME","TEST","batu","gunting","NONE")

        beforeFeature {
            InstantRuleExecution.start()
            TrampolineSchedulerRX.start()
        }

        Scenario("Skenario ambil data Match"){
            Given("set value") {
                given(repository.getMatchById("TEST_ID")).willReturn(Single.just(dummyMatches))
            }

            When("Logic") {
                viewModel.getMatchById("TEST_ID")
            }

            Then("Hasil Match ada") {
                viewModel.matchData.test().assertHasValue()
            }
        }

        Scenario("Skenario ambil data User"){
            Given("set value") {
                given(repository.getUserById("TEST_ID")).willReturn(Single.just(dummyUser))
            }

            When("Logic") {
                viewModel.getUserById("TEST_ID")
            }

            Then("Hasil User ada") {
                viewModel.userData.test().assertHasValue()
            }
        }

        Scenario("Janken Logic Batu VS Gunting") {
            Given("set value") {
                given(repository.jankenLogic("batu","gunting",dummyMatch.player1Username,"P2NAME")).willReturn(Single.just("P1NAME Menang"))
            }

            When("Logic") {
                viewModel.jankenLogic(dummyMatch,"P2NAME")
            }

            Then("Hasil ada dan sesuai") {
                viewModel.jankenLogicResult.test().assertValue {
                    it == "P1NAME Menang"
                }
            }
        }

    }
})
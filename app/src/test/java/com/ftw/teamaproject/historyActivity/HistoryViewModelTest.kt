package com.ftw.teamaproject.historyactivity

import androidx.lifecycle.Observer
import com.example.witachapter9.model.Matches
import com.ftw.teamaproject.helper.InstantRuleExecution
import com.ftw.teamaproject.helper.TrampolineSchedulerRX
import com.ftw.teamaproject.history.viewmodel.HistoryRepo
import com.ftw.teamaproject.history.viewmodel.HistoryViewModel
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class HistoryViewModelTest : Spek({
    Feature("HistoryActivity") {// mnungin gaya
        val repository = mock<HistoryRepo>()
        val viewModel = HistoryViewModel(repository)
        val observeState = mock<Observer<MutableList<com.ftw.teamaproject.model.Matches>>>()

        beforeFeature {
            InstantRuleExecution.start()
            TrampolineSchedulerRX.start()

            viewModel.getMatchesState.observeForever(observeState)
        }

        Scenario("Skenario ambil data history") {
//            val fb = mock<FirebaseFirestore>()
            val r = mock<HistoryRepo>()

            Given("set value") {
                val dummy = mock<MutableList<com.ftw.teamaproject.model.Matches>>()
                given(r.getMatchesData("ID")).willReturn(Single.just(dummy))
            }


            val t = HistoryViewModel(r)
            When("logic") {// lagi makan iya,nah lo
                t.fetchMatchesData("ID")
            }

            Then("Data history ada") {
                verify(r).getMatchesData("ID")

                t.getMatchesState.test().assertHasValue()
            }
        }

        afterFeature {
            InstantRuleExecution.tearDown()
            TrampolineSchedulerRX.tearDown()
        }
    }
})
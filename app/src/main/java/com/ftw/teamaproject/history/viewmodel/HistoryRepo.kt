package com.ftw.teamaproject.history.viewmodel

import android.util.Log
import com.ftw.teamaproject.db
import com.google.firebase.firestore.ktx.toObject
import io.reactivex.Single

class HistoryRepo() {


    fun getMatchesData(myId: String): Single<MutableList<com.ftw.teamaproject.model.Matches>> =
        Single.create {
            db.collection("matches")
                .whereEqualTo("idP1", myId)
                .get()
                .addOnSuccessListener { result ->
                    if (result != null) {
                        val listData = mutableListOf<com.ftw.teamaproject.model.Matches>()
                        for (document in result) {
                            val match = document.toObject<com.ftw.teamaproject.model.Matches>()
                            if (match.winner != "") {
                                listData.add(match)
                            }
                        }
                        Log.d("List Data", listData.toString())
                        it.onSuccess(listData)
                    }
                }
        }
}
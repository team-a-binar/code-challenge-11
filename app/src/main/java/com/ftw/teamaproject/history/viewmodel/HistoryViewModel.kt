package com.ftw.teamaproject.history.viewmodel

import android.os.Environment
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import java.io.FileOutputStream

class HistoryViewModel(private val repo: HistoryRepo) : ViewModel() {


    private val TAG = HistoryViewModel::class.java.simpleName
    private val compositeDisposable = CompositeDisposable()
    val getMatchesState = MutableLiveData<MutableList<com.ftw.teamaproject.model.Matches>>()

    fun fetchMatchesData(myId:String){
//        val mutableData = MutableLiveData<MutableList<Matches>>()
        compositeDisposable.add(
            repo.getMatchesData(myId).subscribe({ data ->
                getMatchesState.postValue(data)
            }, {
                Log.d(TAG, "getMatch: Error ${it.message}")
            })
        )
//        repo.getMatchesData(myId).observeForever {matchesList ->
//            mutableData.value = matchesList
//        }
//        return mutableData
    }

    class Factory(private val repoHistory: HistoryRepo) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            try {
                @Suppress("UNCHECKED_CAST")
                return HistoryViewModel(repoHistory) as T
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
    }
    fun createPDF(data: MutableList<com.ftw.teamaproject.model.Matches>){
        val document = Document(PageSize.A4)
        val table = PdfPTable(floatArrayOf(3f,3f,3f))
        table.defaultCell.horizontalAlignment = Element.ALIGN_CENTER
        table.defaultCell.fixedHeight = 50f
        table.totalWidth = PageSize.A4.width
        table.defaultCell.verticalAlignment = Element.ALIGN_MIDDLE

        table.addCell("Mode")
        table.addCell("Nama")
        table.addCell("Result")

        table.headerRows = 1

        println("Cells = ${table.rows.size}")

        val cells = table.getRow(0).cells
        for (j in cells.indices){
            cells[j].backgroundColor = BaseColor.GRAY
        }

        for (model in data) {
            val modeMatches = model.mode
            val namaPemain = model.player1Username
            val result = model.winner

            table.addCell(modeMatches)
            table.addCell(namaPemain)
            table.addCell(result)
        }
        previewPDF(document, table, data)
    }

    fun previewPDF(document: Document, table: PdfPTable, data: MutableList<com.ftw.teamaproject.model.Matches>){
        val pdfName = "History.pdf"
        val docsFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val pdfFile = File(docsFolder.absolutePath,pdfName)
        val output = FileOutputStream(pdfFile)
        PdfWriter.getInstance(document, output)

        document.open()

        val f = Font(
            Font.FontFamily.HELVETICA,
            30.0f,
            Font.UNDERLINE,
            BaseColor.BLACK
        )
        val g = Font(Font.FontFamily.HELVETICA, 20.0f, Font.NORMAL, BaseColor.BLUE)

        document.add(Paragraph("History Permainan User \n \n", f))
        document.add(table)

        document.close()
        Log.d("Data Table", data.toString())
    }
}
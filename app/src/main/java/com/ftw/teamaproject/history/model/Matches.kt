package com.example.witachapter9.model

import com.google.firebase.Timestamp

data class Matches(
        val p2Choice: String? = null,
        val idMatch: String? = null,
        val idP1: String? = null,
        val image: String? = null,
        val mode: String? = null,
        val p1Choice: String?= null,
        val timestamp: Timestamp? = null,
        val player1Username: String? = null,
        val winner: String? = null
)
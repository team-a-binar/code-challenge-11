package com.example.witachapter9.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ftw.teamaproject.R
import kotlinx.android.synthetic.main.item_history.view.*
import java.text.SimpleDateFormat
import java.util.*

class HistoryAdapter(private val context: Context) :
    RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    private val dataList: MutableList<com.ftw.teamaproject.model.Matches> = mutableListOf()
    fun setListData(data: MutableList<com.ftw.teamaproject.model.Matches>) {
        this.dataList.clear()
        this.dataList.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (dataList.size > 0) {
            dataList.size
        } else {
            0
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val matches = dataList[position]
        holder.bind(matches)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(matches: com.ftw.teamaproject.model.Matches) {

            fun Date?.parseDate(): String {
                val inputFormat = matches.timestamp
                val date = inputFormat?.toDate()
                val outputFormat = SimpleDateFormat("dd/MM/yyyy", Locale("ID"))
                return outputFormat.format(date)
            }

            itemView.tv_date_history.text = matches.timestamp?.toDate().parseDate()
            itemView.tv_mode_history.text = matches.mode
            itemView.tv_result_history.text = matches.winner
            itemView.username.text = matches.player1Username

            //load image
            Glide.with(context).load(matches.imgUser).into(itemView.img_prof)
        }
    }
}
package com.example.witachapter9.ui

interface InterfaceHistory {
    fun showLoading()
    fun hideLoading()
}
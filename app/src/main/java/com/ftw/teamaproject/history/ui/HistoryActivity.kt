package com.example.witachapter9.ui

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.witachapter9.adapter.HistoryAdapter
import com.ftw.teamaproject.R
import com.ftw.teamaproject.history.viewmodel.HistoryViewModel
import com.ftw.teamaproject.model.Matches
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_history.*
import org.koin.android.ext.android.inject
import java.io.File


class HistoryActivity : AppCompatActivity(), InterfaceHistory {

    private val sharedPreference by lazy {
        getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }
    private var myId = ""
    private lateinit var adapter: HistoryAdapter
    private val factory: HistoryViewModel.Factory by inject()
    private lateinit var viewModel: HistoryViewModel
    private val data = mutableListOf<Matches>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) { /* ... */
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) { /* ... */
                }
            }).check()
        viewModel = ViewModelProvider(this, factory).get(HistoryViewModel::class.java)

        myId = sharedPreference.getString("PLAYERID", "SP_NOT_FOUND")!!

        adapter = HistoryAdapter(this)
        rv_history_battle.layoutManager = LinearLayoutManager(this)
        rv_history_battle.adapter = adapter
        observeData()
        viewModel.fetchMatchesData(myId)

        btn_convert.setOnClickListener {
            //proccess to make PDF
            val docsFolder =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            if (!docsFolder.exists()) {
                docsFolder.mkdir()
                Log.i("History", "Membuat Direktori baru untuk PDF History")
            } else {
                viewModel.createPDF(data)

                //process to open file from snackbar
                val file = File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                        .absolutePath + "/History.pdf"
                )
                val uriPdf = FileProvider.getUriForFile(
                    applicationContext,
                    applicationContext.packageName + ".provider",
                    file
                )
                val snack = Snackbar.make(
                    it,
                    "PDF Exported on ${docsFolder.absolutePath}/History.pdf",
                    Snackbar.LENGTH_LONG
                )
                snack.setActionTextColor(Color.RED)
                snack.setAction("OPEN PDF", View.OnClickListener {
                    val target = Intent(Intent.ACTION_VIEW)
                    target.setDataAndType(uriPdf, "application/pdf")
                    target.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

                    val intent = Intent.createChooser(target, "Open File")
                    try {
                        startActivity(intent)
                    } catch (e: ActivityNotFoundException) {
                        Toast.makeText(this, "No PDF Reader in your phone", Toast.LENGTH_SHORT)
                            .show()
                    }
                }).show()

            }
        }
        btn_back.setOnClickListener {
            finish()
        }
    }

    private fun observeData() {
        viewModel.getMatchesState.observe(this, Observer {
            this.data.addAll(it)
            adapter.setListData(it)
            adapter.notifyDataSetChanged()
        })
    }

    override fun showLoading() {
        rv_history_battle.visibility = View.GONE
        tv_no_data_history.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        rv_history_battle.visibility = View.VISIBLE
        tv_no_data_history.visibility = View.GONE

    }
}
package com.example.cc9_single_mainmenu.singleplayermvvm

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.cc9_single_mainmenu.mainmenu.MainMenuActivityDummy
import com.example.fiturprofil.ui.mainmenu.MainMenuActivity
import com.ftw.teamaproject.R
import kotlinx.android.synthetic.main.activity_result_single_player.*

class ResultSingleActivity : AppCompatActivity() {
    private val TAG = ResultSingleActivity::class.java.simpleName
    private var pilihanPlayer = ""
    private var pilihanKomputer = ""
    private var hasilMain = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_single_player)

        hasilMain = intent.extras!!.getString("data winner", "")
        tv_resultsingleplay.text = hasilMain
        Log.d(TAG, "Success show result: $hasilMain")

        iv_resetsingleplay.setOnClickListener {
            val intent = Intent(this, SingleActivity::class.java)
            startActivity(intent)
            finish()
        }

        pilihanPlayer = intent.extras!!.getString("pilihan pemain", "")
        pilihanKomputer = intent.extras!!.getString("pilihan komputer", "")

        when (pilihanPlayer) {
            "Batu" -> {
                iv_playerresult.setImageResource(R.drawable.batux)
            }
            "Gunting" -> {
                iv_playerresult.setImageResource(R.drawable.guntingx)
            }
            else -> {
                iv_playerresult.setImageResource(R.drawable.kertasx)
            }
        }

        when (pilihanKomputer) {
            "Batu" -> {
                iv_compresult.setImageResource(R.drawable.batux)
            }
            "Gunting" -> {
                iv_compresult.setImageResource(R.drawable.guntingx)
            }
            else -> {
                iv_compresult.setImageResource(R.drawable.kertasx)
            }
        }

        iv_backsingleplay2.setOnClickListener {
            val intent = Intent(this, MainMenuActivity::class.java)
            startActivity(intent)
        }
    }
}

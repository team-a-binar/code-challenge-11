package com.ftw.teamaproject.SinglePlayer.model

import com.google.firebase.firestore.ServerTimestamp

data class MatchesDummy(
    val idMatch: String? = null,
    var idP1: String? = null,
    var mode: String? = null,
    var p1Choice: String? = null,
    var p2Choice: String? = null,
    var player1Username: String? = null,
    var timestamp: ServerTimestamp? = null,
    var winner: String? = null
)

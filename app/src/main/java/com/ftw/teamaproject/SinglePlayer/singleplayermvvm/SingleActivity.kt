package com.example.cc9_single_mainmenu.singleplayermvvm

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.fiturprofil.ui.mainmenu.MainMenuActivity
import com.ftw.teamaproject.R
import com.ftw.teamaproject.SinglePlayer.model.MatchesDummy
import com.ftw.teamaproject.model.Matches
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_single_player.*
import kotlin.random.Random

class SingleActivity : AppCompatActivity() {
//    private val sharedPreferences by lazy {
//        getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
//    }
//    private val editor by lazy {
//        sharedPreferences.edit()
//    }
    private lateinit var viewModel: SingleViewModel
    private var kondPertama = true
    var pilPlayer = ""
    var pilComp = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_player)

        FirebaseApp.initializeApp(this)

        viewModel = ViewModelProvider(this, SingleViewModel.Factory(Repository())).get(SingleViewModel::class.java)

        val pilihanPlay = listOf(batu1, gunting1, kertas1)
        val pilihanComp = listOf("Batu", "Gunting", "Kertas")
        var random = Random.nextInt(0, 2)
        val showReset = "Success reset"

        batu1.background = getDrawable(android.R.color.transparent)
        gunting1.background = getDrawable(android.R.color.transparent)
        kertas1.background = getDrawable(android.R.color.transparent)
        Log.d("Button reset", showReset)

        for (btn in pilihanPlay) {
            btn.setOnClickListener {
                if (kondPertama) {
                    it.background = getDrawable(R.drawable.klikbaru)
                    pilPlayer = it.contentDescription.toString()
                    pilComp = pilihanComp.random()

                    val hasilnya = viewModel.cekHasil(pilPlayer, pilComp)

                    kondPertama = false
                    Log.d("Player has chosen", pilPlayer)
                    Log.d("Computer has chosen", pilComp)

                    val intent = Intent(
                        this@SingleActivity,
                        ResultSingleActivity::class.java
                    )

                    viewModel.updateMatch(
                        p1Choice = pilPlayer,
                        computerChoice = pilComp,
                        winner = hasilnya
                    )

                    intent.putExtra("pilihan pemain", pilPlayer)
                    intent.putExtra("pilihan komputer", pilComp)
                    intent.putExtra("data winner", hasilnya)
                    startActivity(intent)

                    viewModel.updateMatchState
                        .observe(this, Observer {data ->
                            if (data == MatchesDummy()) {
                                Toast.makeText(this, "Success update data", Toast.LENGTH_LONG)
                                    .show()
                            } else {
                                Toast.makeText(this, "Failed update data", Toast.LENGTH_LONG)
                                    .show()
                            }
                        })

                } else {
                    Toast.makeText(baseContext, "Can't select more", Toast.LENGTH_SHORT).show()
                }
            }
        }

        iv_backsingleplay.setOnClickListener {
            val intent = Intent(this, MainMenuActivity::class.java)
            startActivity(intent)
        }
    }
}
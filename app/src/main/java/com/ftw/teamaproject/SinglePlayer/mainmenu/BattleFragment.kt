package com.example.cc9_single_mainmenu.mainmenu

import android.content.Intent
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.cc9_single_mainmenu.singleplayermvvm.SingleActivity
import com.ftw.teamaproject.R
import com.ftw.teamaproject.multiPlayerMenu.MultiPlayerMenuActivity
import kotlinx.android.synthetic.main.fragment_battle.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BattleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BattleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_battle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        enterTransition = TransitionInflater.from(activity).inflateTransition(R.transition.fade_transition)

        iv_singleplayer.setOnClickListener {
            val intent = Intent(activity, SingleActivity::class.java)
            startActivity(intent)
        }

        iv_multiplayer.setOnClickListener {
            val intent = Intent(activity, MultiPlayerMenuActivity::class.java)
            startActivity(intent)
        }
    }
}
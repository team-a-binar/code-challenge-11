package com.example.cc9_single_mainmenu

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.example.cc9_single_mainmenu.singleplayermvvm.Repository
import com.example.cc9_single_mainmenu.singleplayermvvm.SingleViewModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.koin.core.context.startKoin

import org.koin.dsl.module

class AppLoader : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

//        startKoin(this, listOf(module {
//            single {
//                Repository()
//            }
//
//            single {
//                SingleViewModel.Factory(get())
//            }
//        }))

    }
}

val db = Firebase.firestore
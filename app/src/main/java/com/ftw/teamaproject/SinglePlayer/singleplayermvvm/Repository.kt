package com.example.cc9_single_mainmenu.singleplayermvvm

import android.util.Log
import com.example.cc9_single_mainmenu.db
import com.ftw.teamaproject.SinglePlayer.model.MatchesDummy
import com.ftw.teamaproject.model.Matches
import com.google.firebase.firestore.FieldValue
import io.reactivex.Single

class Repository {
    private var id = ""
    private var TAG = Repository::class.java.simpleName

    fun updateMatches(
        p1Choice: String,
        computerChoice: String,
        winner: String
    ): Single<MatchesDummy> = Single.create {

        //val documentReference =

        db.collection("matches")
            .add(
                hashMapOf(
                    "idMatch" to "",
                    "idP1" to "",
                    "mode" to "singleplayer",
                    "p1Choice" to p1Choice,
                    "p2Choice" to computerChoice,
                    "player1Username" to "",
                    "timestamp" to FieldValue.serverTimestamp(),
                    "winner" to winner
                )
            ).addOnSuccessListener { documentReference ->
                val listData = MatchesDummy()
                it.onSuccess(listData)

//                if (documentReference != null){
//                    val model = documentReference.
//                    listData.add(model)
//                }

                id = documentReference.id
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                Log.d(TAG, "Player choose: $p1Choice")
                Log.d(TAG, "Computer choose: $computerChoice")
                Log.d(TAG, "The winner is: $winner ")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }
}
package com.example.cc9_single_mainmenu.singleplayermvvm

import android.util.Log
import androidx.lifecycle.*
import com.ftw.teamaproject.SinglePlayer.model.MatchesDummy
import com.ftw.teamaproject.model.Matches
import io.reactivex.disposables.CompositeDisposable

class SingleViewModel(private val repoSingle: Repository) : ViewModel() {
    private val TAG = SingleViewModel::class.java.simpleName
    private val compositeDisposable = CompositeDisposable()
    val updateMatchState = MutableLiveData<MatchesDummy>()
    private val suit = mutableListOf("Batu", "Gunting", "Kertas")

    fun cekHasil(pertama: String, kedua: String): String {
        var winner = ""

        if (pertama == suit[0] && kedua == suit[1] ||
            pertama == suit[1] && kedua == suit[2] ||
            pertama == suit[2] && kedua == suit[0]
        ) {
            winner = "Player WIN"
            Log.d(TAG, "The winner is PLAYER")
        } else if (
            pertama == suit[0] && kedua == suit[2] ||
            pertama == suit[1] && kedua == suit[0] ||
            pertama == suit[2] && kedua == suit[1]
        ) {
            winner = "Computer WIN"
            Log.d(TAG, "The winner is COMPUTER")
        } else {
            winner = "DRAW"
            Log.d(TAG, "The result is DRAW")
        }

        return winner
    }

    fun updateMatch(
        p1Choice: String,
        computerChoice: String,
        winner: String
    ) {
        compositeDisposable.add(
            repoSingle.updateMatches(p1Choice, computerChoice, winner).subscribe({ data ->
                updateMatchState.postValue(data)
            }, {
                Log.d(TAG, "updateMatch: Error ${it.message}")
            })
        )
    }

    class Factory(private val repoSingle: Repository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            try {
                @Suppress("UNCHECKED_CAST")
                return SingleViewModel(repoSingle) as T
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
    }
}
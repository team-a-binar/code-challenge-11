package com.ftw.teamaproject.model

import com.google.firebase.Timestamp

data class NotificationModel(
    var data: Map<String, String> = mutableMapOf(),
    var to: String = "",
    var date: Timestamp? = null
)
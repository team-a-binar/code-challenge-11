package com.ftw.teamaproject.model

import android.os.Parcel
import android.os.Parcelable

data class User(
    var id: String = "",
    val username: String = "",
    val email: String = "",
    val password: String = "",
    var tokenFcm: String = "",
    val image: String = "",
    var friends: MutableList<String> = mutableListOf(),
    var match_new: MutableList<String> = mutableListOf(),
    val token: String = "",
    val point: Int = 0,
    var isAuthenticated: Boolean = false
)
package com.ftw.teamaproject.model

import com.google.firebase.Timestamp

data class Matches(
    var idMatch: String = "",
    var idP1: String = "",
    var idP2: String = "",
    var player1Username: String = "",
    var mode: String = "",
    var p1Choice: String = "",
    var p2Choice: String = "",
    var winner: String = "",
    var imgUser: String = "",
    val timestamp: Timestamp = Timestamp.now()
)
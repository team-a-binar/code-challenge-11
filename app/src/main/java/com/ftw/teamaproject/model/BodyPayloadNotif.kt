package com.ftw.resultgame.model

import com.google.firebase.firestore.FieldValue

data class BodyPayloadNotif(
    val to: String? = null,
    val date: FieldValue? = null,
    val data: Data? = null
)

data class Data(
    val idPengirim: String? = null,
    val mode: String? = null,
    val title: String? = null,
    val message: String? = null
)
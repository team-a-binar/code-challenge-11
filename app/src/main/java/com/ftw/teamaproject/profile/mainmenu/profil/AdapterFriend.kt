package com.example.fiturprofil.ui.mainmenu.profil

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ftw.teamaproject.R
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.profile.mainmenu.Users
import kotlinx.android.synthetic.main.layout_friend.view.*

class AdapterFriend(
    private val data: MutableList<User> = mutableListOf()
): RecyclerView.Adapter<AdapterFriend.listFriendHolder>() {
    inner class listFriendHolder(private val view: View): RecyclerView.ViewHolder(view){
        fun bind(users: User){
            view.tv_nama_teman.text = users.username
            view.tv_poin_teman.text = users.point.toString()
        }
    }
    fun setData(data: MutableList<User>){
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterFriend.listFriendHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_friend, parent,false)
        return listFriendHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: AdapterFriend.listFriendHolder, position: Int) {
        holder.bind(data[position])
    }
}
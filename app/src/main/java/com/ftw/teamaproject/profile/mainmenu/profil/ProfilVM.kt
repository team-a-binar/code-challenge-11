package com.example.fiturprofil.ui.mainmenu.profil

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.profile.mainmenu.Repo

class ProfilVM : ViewModel() {

    private val repo = Repo()

    fun getDataFriend(myId: String): LiveData<MutableList<User>> {
        val mutableData = MutableLiveData<MutableList<User>>()
        repo.getUserFriend(myId).observeForever {
            mutableData.value = it
        }
        return mutableData
    }

    fun getUser(myId: String): LiveData<MutableList<User>> {
        val data = MutableLiveData<MutableList<User>>()
        repo.getUserInfo(myId).observeForever {
            data.value = it
        }
        return data
    }

    fun getFriend(nama: String): LiveData<MutableList<User>> {
        val data = MutableLiveData<MutableList<User>>()
        repo.getUserName(nama).observeForever {
            data.value = it
        }
        return data
    }

    fun updateUser(
        usernameUpdate: String,
        emailUpdate: String,
        passwordUpdate: String,
        image: String,
        myId: String
    ) {
        repo.updateProfi(
            myId = myId,
            emailUpdate = emailUpdate,
            usernameUpdate = usernameUpdate,
            passwordUpdate = passwordUpdate,
            image = image
        )
    }

    fun sendNotif(myId: String, destToken: String, username: String, friendName: String) {
        repo.sendNotif(myId, destToken, username, friendName)

    }

}
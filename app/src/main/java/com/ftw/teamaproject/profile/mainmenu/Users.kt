package com.ftw.teamaproject.profile.mainmenu

class Users(
    var id: String = "",
    var username: String = "",
    var email: String = "",
    var image: String = "",
    var point: Int? = null,
    var friends: MutableList<String> = mutableListOf()
)
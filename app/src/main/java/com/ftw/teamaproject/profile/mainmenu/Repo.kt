package com.ftw.teamaproject.profile.mainmenu

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ftw.resultgame.model.BodyPayloadNotif
import com.ftw.resultgame.model.Data
import com.ftw.resultgame.network.ApiServices
import com.ftw.teamaproject.BuildConfig
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.util.Constants
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class Repo {


    //    private val myId = "79RnEHnUoDcFypGiiCpZr2SLcMp2" //preference id
    private val TAG = "REPOSITORY"


    private fun servicesNotif(): ApiServices {
        val client = OkHttpClient().newBuilder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG)
                    HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            })
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_NOTIF)
            .client(client)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(ApiServices::class.java)
    }

    private fun pushNotif(bodyPayloadNotif: BodyPayloadNotif) = servicesNotif()
        .pushNotif(Constants.SERVER_KEY, bodyPayloadNotif)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())


    fun getUserFriend(myId: String): LiveData<MutableList<User>> {
        val mutableData = MutableLiveData<MutableList<User>>()

        db.collection("users")
            .whereEqualTo("id", myId) //preference id
            .addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.d(TAG, "Failed Upload Data", e)
                    return@addSnapshotListener
                }
                if (snapshot != null && !snapshot.isEmpty) {
                    Log.d(TAG, snapshot.size().toString())
                    val model = snapshot.toObjects<User>().toMutableList()
                    val friends = mutableListOf<User>()

                    GlobalScope.launch(Dispatchers.IO) {
                        model[0].friends.forEach {
                            val friend = getUserById(it)
                            friend?.let { it1 ->
                                friends.add(it1)
                            }
                        }
                        launch(Dispatchers.Main) {
                            mutableData.value = friends
                        }
                    }
                }
            }
        return mutableData
    }

    private suspend fun getUserById(id: String): User? {
        var user: User? = null
        db.collection("users")
            .document(id)
            .get()
            .addOnSuccessListener {
                Log.d("INFO USER", "Succes Get ID User :$id")
                val model = it.toObject<User>()
                if (model != null) {
                    user = model
                }
            }
            .addOnFailureListener {
                Log.d("INFO USER", "Gagal mendapatkan ID User :$id")
            }
            .await()
        return user
    }

    fun getUserInfo(myId: String): LiveData<MutableList<User>> {
        var pemain = MutableLiveData<MutableList<User>>()
        db.collection("users")
            .whereEqualTo("id", myId) //preference id
            .addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.d(TAG, "Failed Upload Data", e)
                    return@addSnapshotListener
                }
                if (snapshot != null && !snapshot.isEmpty) {
                    Log.d(TAG, snapshot.size().toString())
                    val model = snapshot.toObjects<User>().toMutableList()
                    pemain.value = model
                }
            }
        return pemain
    }

    fun updateProfi(
        myId: String,
        usernameUpdate: String,
        emailUpdate: String,
        passwordUpdate: String,
        image: String
    ): LiveData<MutableList<User>> {
        val user = MutableLiveData<MutableList<User>>()
        db.collection("users")
            .document(myId) //preferenceid
            .update(
                mapOf(
                    "username" to usernameUpdate,
                    "email" to emailUpdate,
                    "password" to passwordUpdate,
                    "image" to image
                )
            )
        return user
    }

    @SuppressLint("CheckResult")
    fun sendNotif(myId: String, destToken: String, username: String, friendName: String) {
        val dataNotif = BodyPayloadNotif(
            to = destToken,
            date = FieldValue.serverTimestamp(),
            data = Data(
                idPengirim = myId,
                mode = "Friendlist",
                title = "$username menambahkan anda sebagai teman",
                message = "Permintaan pertemanan dari $username"
            )
        )
        pushNotif(dataNotif).subscribe({ it ->
            db.collection("notification")
                .document(username)
                .collection("data")
                .add(dataNotif)

        }, {
            Log.d(TAG, "sendNotif: Fail because ${it.message}")
        })

    }

    fun getUserName(nama: String): LiveData<MutableList<User>> {
        var pemain = MutableLiveData<MutableList<User>>()
        db.collection("users")
            .whereEqualTo("username", nama) //preference id
            .addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.d(TAG, "Failed Search Username", e)
                    return@addSnapshotListener
                }
                if (snapshot != null && !snapshot.isEmpty) {
                    Log.d(TAG, snapshot.size().toString())
                    val model = snapshot.toObjects<User>().toMutableList()
                    pemain.value = model
                } else {
                    pemain.value = snapshot?.toObjects<User>()?.toMutableList()
                }
            }
        return pemain
    }

}
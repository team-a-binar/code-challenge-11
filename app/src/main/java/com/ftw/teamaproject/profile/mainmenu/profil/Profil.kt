package com.ftw.teamaproject.profile.mainmenu.profil

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.transition.TransitionInflater
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.fiturprofil.ui.mainmenu.profil.AdapterFriend
import com.example.fiturprofil.ui.mainmenu.profil.ProfilVM
import com.ftw.teamaproject.R
import com.ftw.teamaproject.auth
import com.ftw.teamaproject.storageRef
import com.google.firebase.auth.EmailAuthProvider
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.dialog_edit_profile.view.*
import kotlinx.android.synthetic.main.dialog_search_friend.view.*
import kotlinx.android.synthetic.main.fragment_profil.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayOutputStream

class Profil : Fragment() {

    private lateinit var adapter: AdapterFriend

    //    private val DAP = AdapterFriend::class.java.simpleName
    private var myId = ""
    private var usernamePlayer = ""
    private var emailPlayer = ""
    private var defaultServerPathPhoto = ""
    private var targetTokenFcm = ""
    private lateinit var username: TextView
    private lateinit var email: TextView
    private var imageUri: Uri? = null
    private lateinit var poin: TextView
    private val sharedPreference by lazy {
        activity?.getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }
    private val viewModel by lazy {
        ViewModelProvider(this).get(ProfilVM::class.java)
    }
    private val dialogUpdate by lazy {
        layoutInflater.inflate(R.layout.dialog_edit_profile, null, false)
    }

    private val dialogSearch by lazy {
        layoutInflater.inflate(R.layout.dialog_search_friend, null, false)
    }
    private lateinit var showDialog: AlertDialog
    private lateinit var showDialogSearch: AlertDialog
    private var usernameTarget = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //preference
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profil, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        enterTransition =
            TransitionInflater.from(activity).inflateTransition(R.transition.fade_transition)

        myId = sharedPreference?.getString("PLAYERID", "SP_NOT_FOUND")!!
        usernamePlayer = sharedPreference?.getString("PLAYER_USERNAME", "SP_NOT_FOUND")!!
        emailPlayer = sharedPreference?.getString("EMAIL", "SP_NOT_FOUND")!!
        adapter = AdapterFriend()
        rv_list_friend.layoutManager = LinearLayoutManager(context)
        rv_list_friend.setHasFixedSize(true)
        rv_list_friend.adapter = adapter

        observeDataFriend()

        username = tv_nama
        email = tv_email_pemain
        poin = tv_poin_pemain

        observeDataUser()

        btn_edit_profil.setOnClickListener {
            showDialogUpdate()
        }

        iv_search.setOnClickListener {
            showDialogSearchFriend()
        }
    }

    fun observeDataFriend() {
        viewModel.getDataFriend(myId).observe(viewLifecycleOwner, Observer {
            adapter.setData(it)
        })
    }

    fun observeDataUser() {
        val ref = storageRef.child("/image.jpg")
        viewModel.getUser(myId).observe(viewLifecycleOwner, Observer {
            CoroutineScope(Dispatchers.IO).launch {
                val photoUri = ref.downloadUrl.await()
                launch(Dispatchers.Main) {
                    username.text = it[0].username
                    email.text = it[0].email
                    poin.text = it[0].point.toString()
                    Glide.with(this@Profil).load(photoUri).into(iv_profil)
                }
            }
        })
    }

    private fun observeDataUsernameFriend() {
        val username = dialogSearch.search_nama.text.toString()
        viewModel.getFriend(username).observe(viewLifecycleOwner, Observer {
            val fadeInAnim = AnimationUtils.loadAnimation(requireActivity(), R.anim.fadein)
            val fadeOutAnim = AnimationUtils.loadAnimation(requireActivity(), R.anim.fadeout)
            if (it.isNullOrEmpty()) {
                dialogSearch.btn_add_friend.startAnimation(fadeOutAnim)
                Thread.sleep(500)
                dialogSearch.btn_add_friend.clearAnimation()
                dialogSearch.btn_add_friend.visibility = View.GONE
                dialogSearch.tv_search_friend.startAnimation(fadeInAnim)
                dialogSearch.tv_search_friend.text = "Username tidak ditemukan"
            } else {
                dialogSearch.tv_search_friend.startAnimation(fadeOutAnim)
                dialogSearch.btn_add_friend.startAnimation(fadeOutAnim)
                Thread.sleep(500)
                dialogSearch.btn_add_friend.startAnimation(fadeInAnim)
                dialogSearch.tv_search_friend.startAnimation(fadeInAnim)
                dialogSearch.btn_add_friend.visibility = View.VISIBLE
                usernameTarget = it[0].username
                targetTokenFcm = it[0].tokenFcm
                dialogSearch.tv_search_friend.text = usernameTarget
                Log.d("NAMA", it[0].username)
            }
        })
    }

    fun showDialogUpdate() {
        this.let {
            if (dialogUpdate.parent != null) {
                val viewGroup = dialogUpdate.parent as ViewGroup
                viewGroup.removeView(dialogUpdate)
            }
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setView(dialogUpdate)
            showDialog = dialog.create()
            showDialog.show()

            dialogUpdate.et_username.setText(usernamePlayer)
            dialogUpdate.et_email.setText(emailPlayer)

            dialogUpdate.btn_update_profile.setOnClickListener {

                if (dialogUpdate.et_username.text.isNullOrEmpty()) {
                    dialogUpdate.et_username.error = "Username harap diisi"
                    dialogUpdate.et_username.isFocusable = true
                } else if (dialogUpdate.et_password.text.isNullOrEmpty()) {
                    dialogUpdate.et_password.error = "Password baru harap diisi"
                    dialogUpdate.et_password.isFocusable = true
                } else if (dialogUpdate.et_old_password.text.isNullOrEmpty()) {
                    dialogUpdate.et_old_password.error = "Password lama harap diisi"
                    dialogUpdate.et_old_password.isFocusable = true
                } else if (dialogUpdate.et_email.text.isNullOrEmpty()) {
                    dialogUpdate.et_email.error = "Email harap diisi"
                    dialogUpdate.et_email.isFocusable = true
                } else if (!Patterns.EMAIL_ADDRESS.matcher(dialogUpdate.et_email.text.toString())
                        .matches()
                ) {
                    dialogUpdate.et_email.error = "Masukkan format email yang benar"
                    dialogUpdate.et_email.isFocusable = true
                } else {
                    observeDataUpdate()
                    showDialog.dismiss()
                }

            }
            dialogUpdate.btn_click.setOnClickListener {
                CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(requireContext(), this@Profil)
            }
        }
    }

    fun showDialogSearchFriend() {
        this.let {
            if (dialogSearch.parent != null) {
                val viewGroup = dialogSearch.parent as ViewGroup
                viewGroup.removeView(dialogSearch)
            }

            val dialog = AlertDialog.Builder(requireContext())
            dialog.setView(dialogSearch)
            showDialogSearch = dialog.create()
            showDialogSearch.show()

            dialogSearch.btn_search.setOnClickListener {
                dialogSearch.layoutFriendName.visibility = View.VISIBLE
                observeDataUsernameFriend() //fungsi nampilin username
            }

            dialogSearch.btn_add_friend.setOnClickListener {
                if (usernameTarget != "") {
                    viewModel.sendNotif(
                        myId,
                        targetTokenFcm,
                        usernamePlayer,
                        usernameTarget
                    ) //fungsi ngirim notif

                    Toast.makeText(
                        requireActivity(),
                        "Permintaan pertemanan sudah terkirim",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Log.d("NAMA KOSONG", "Nama tujuan : usernameTarget")
                    Toast.makeText(
                        requireActivity(),
                        "Username $usernameTarget tidak ditemukan",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                showDialogSearch.dismiss()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result = CropImage.getActivityResult(data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                imageUri = result.uri
                Log.d("IMAGE", imageUri.toString())
                Glide.with(this@Profil).load(imageUri).into(dialogUpdate.update_foto)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        }
    }

    fun observeDataUpdate() {
        val usernameUpdate = dialogUpdate.et_username?.text.toString()
        val emailUpdate = dialogUpdate.et_email?.text.toString()
        val passwordUpdate = dialogUpdate.et_password?.text.toString()
        val oldPassword = dialogUpdate.et_old_password?.text.toString()

        val credential = EmailAuthProvider
            .getCredential(emailPlayer, oldPassword)

        // Prompt the user to re-provide their sign-in credentials
        auth.currentUser?.reauthenticate(credential)
            ?.addOnCompleteListener {
                if (it.isSuccessful) {
                    // Sign in success now update email
                    auth.currentUser!!.updateEmail(emailUpdate)
                    //update password
                    auth.currentUser!!.updatePassword(passwordUpdate)
                    Log.d(
                        "Profil",
                        "observeDataUpdate: Email dan Password terganti menjadi $emailUpdate | $passwordUpdate"
                    )
                }
                Log.d("Profil", "User re-authenticated.")

            }?.addOnFailureListener {
                Log.d("Profil", "observeDataUpdate: Gagal login ")
            }


        val mountainRef = storageRef.child("image.jpg")
        val mountainImageRef = storageRef.child("image/image.jpg")

        mountainRef.name == mountainImageRef.name
        mountainRef.path == mountainImageRef.path
        dialogUpdate.update_foto?.isDrawingCacheEnabled = true
        dialogUpdate.update_foto?.buildDrawingCache()

        val bitmap = (dialogUpdate.update_foto.drawable as BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        mountainRef.putBytes(data)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val downloadUri = it.result
                    Log.d("FOTO", "Uri Gambar: $downloadUri")
                }
            }
            .addOnSuccessListener { task ->
                Log.d("Up Foto", "Path : ${task.metadata?.path}")
                Log.d("Up Foto", "URL : ${task.storage.downloadUrl}")
                Log.d("Up Foto", "URL : ${task.storage.bucket}")
                Log.d("Up Foto", "PATH-Storage : ${task.storage.path}")
                Log.d("Up Foto", "Suskes Save Image")
                viewModel.updateUser(
                    usernameUpdate,
                    emailUpdate,
                    passwordUpdate,
                    task.storage.path,
                    myId
                )

                viewModel.getUser(myId).observe(viewLifecycleOwner, Observer {
                    CoroutineScope(Dispatchers.IO).launch {
                        launch(Dispatchers.Main) {
                            username.text = usernameUpdate
                            email.text = emailUpdate
                            storageRef.child(it[0]?.image!!).downloadUrl.addOnSuccessListener { uri ->
                                Glide.with(this@Profil).load(uri).into(iv_profil)
                            }
                        }
                    }
                })
            }
            .addOnFailureListener { e ->
                Log.d("Update foto", "error: ${e.message}")
            }
    }
}
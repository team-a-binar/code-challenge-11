package com.example.fiturprofil.ui.mainmenu

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.cc9_single_mainmenu.mainmenu.BattleFragment
import com.example.cc9_single_mainmenu.mainmenu.NotificationActivity
import com.example.cc9_single_mainmenu.mainmenu.TopScoreFragment
import com.example.witachapter9.ui.HistoryActivity
import com.ftw.teamaproject.R
import com.ftw.teamaproject.auth
import com.ftw.teamaproject.profile.mainmenu.profil.Profil
import com.ftw.teamaproject.ui.landingPage.LandingPageActivity
import com.ftw.teamaproject.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_main_menu.*
import kotlinx.android.synthetic.main.layout_main.*
import kotlinx.android.synthetic.main.main.*

class MainMenuActivity : AppCompatActivity() {

    private lateinit var appBarConf: AppBarConfiguration
    lateinit var battleFrag: BattleFragment
    lateinit var topFrag: TopScoreFragment
    lateinit var profFrag: Profil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(null)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setHomeButtonEnabled(false)
        val navCon = findNavController(R.id.nav_host_fragment)
//        appBarConf = AppBarConfiguration(
//            setOf(
//                R.id.nav_battle,
//                R.id.nav_topscore,
//                R.id.nav_profil
//            ), drawer_layout
//        )
        nav_view.setupWithNavController(navCon)
        button_nav.setupWithNavController(navCon)
        button_nav.itemIconTintList = null

        navCon.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.battle_frag -> {
                    btn_image.setImageResource(0)
                    tv_label.setText("Mode Battle")
                }
                R.id.topscore_frag -> {
                    btn_image.setImageResource(R.drawable.ic_baseline_notifications_24)
                    tv_label.setText("Top Score")
                    btn_image.setOnClickListener {
                        startActivity(Intent(this, NotificationActivity::class.java))
                    }
                }
                R.id.profil_frag -> {
                    btn_image.setImageResource(R.drawable.ic_battle)
                    tv_label.setText("Profil")
                    btn_image.setOnClickListener {
                        startActivity(Intent(this, HistoryActivity::class.java))
                    }
                }
            }

        }
//        button_nav.setOnNavigationItemReselectedListener {
//            when (it.itemId) {
//                R.id.battle_frag -> {
//                    battleFrag = BattleFragment()
//                    supportFragmentManager.beginTransaction()
//                        .replace(R.id.nav_host_fragment, battleFrag)
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit()
//                    btn_image.setImageResource(0)
//                    tv_label.setText("Mode Battle")
//                }
//                R.id.topscore_frag -> {
//                    topFrag = TopScoreFragment()
//                    supportFragmentManager.beginTransaction()
//                        .replace(R.id.nav_host_fragment, topFrag)
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit()
//                    btn_image.setImageResource(R.drawable.ic_baseline_notifications_24)
//                    tv_label.setText("Top Score")
//                    btn_image.setOnClickListener {//activity notif
//                        Intent(this, NotificationActivity::class.java).apply {
//                            startActivity(this)
//                        }
//                    }
//                }
//                R.id.profil_frag -> {
//                    profFrag = Profil()
//                    supportFragmentManager.beginTransaction()
//                        .replace(R.id.nav_host_fragment, profFrag)
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit()
//                    btn_image.setImageResource(R.drawable.ic_battle)
//                    btn_image.setOnClickListener {//activity history
//                        Intent(this, HistoryActivity::class.java).apply {
//                            startActivity(this)
//                        }
//                    }
//                    tv_label.setText("Profil")
//                }
//            }
//        }
        nav_view.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.btn_cancel ->{
                    onBackPressed()
                }
                R.id.logout -> {
                    auth.signOut()
                    Intent(this, LandingPageActivity::class.java).apply {
                        startActivity(this)
                    }
                }
                R.id.exitApps -> {
                    finishAffinity()
                }
            }
            true
        }
        btn_menu.setOnClickListener {
            drawer_layout.openDrawer(GravityCompat.END)
        }
    }

//    override fun onSupportNavigateUp(): Boolean {
//        val navController = findNavController(R.id.nav_host_fragment)
//        return navController.navigateUp() || super.onSupportNavigateUp()
//    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END)
        }
        super.onBackPressed()
    }
}
package com.ftw.teamaproject

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.ftw.teamaproject.services.ActivityLifecycleHandler
import com.ftw.teamaproject.util.myModule
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application(){

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(ActivityLifecycleHandler(this))
        startKoin {
            androidContext(this@App)
            modules(myModule)
        }
    }
}

val db = Firebase.firestore
val auth = FirebaseAuth.getInstance()
val storageRef = FirebaseStorage.getInstance().reference


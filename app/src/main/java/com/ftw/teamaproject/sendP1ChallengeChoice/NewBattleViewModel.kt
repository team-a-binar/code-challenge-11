package com.ftw.teamaproject.sendP1ChallengeChoice

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.sendP1ChallengeChoice.model.PushNotification
import kotlinx.coroutines.launch

class NewBattleViewModel : ViewModel() {
    private val repository = NewBattleRepository()
    
    fun addMatch(matches: Matches, enemyId: String) = viewModelScope.launch {
        repository.addMatch(matches, enemyId)
    }

    fun addDataNotif(notificationData: PushNotification, p2Username: String) = viewModelScope.launch {
        repository.addDataNotif(notificationData,p2Username)
        repository.sendNotification(notificationData)
    }

}

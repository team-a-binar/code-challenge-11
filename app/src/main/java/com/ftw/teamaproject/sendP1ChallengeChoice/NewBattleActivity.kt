package com.ftw.teamaproject.sendP1ChallengeChoice

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.R
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.multiPlayerMenu.MultiPlayerMenuViewModel
import com.ftw.teamaproject.sendP1ChallengeChoice.model.NotificationData
import com.ftw.teamaproject.sendP1ChallengeChoice.model.PushNotification
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FieldValue.serverTimestamp
import kotlinx.android.synthetic.main.send_p1choice.*


class NewBattleActivity : AppCompatActivity() {

    private val sharedPreferences by lazy {
        getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }
    private val editor by lazy {
        sharedPreferences.edit()
    }

    val TAG = NewBattleActivity::class.java.simpleName

    private lateinit var viewModel: NewBattleViewModel

    private var receiveToken = ""
    private var myId = ""
    private var myUsername = ""
    private var player2Username = ""
    private var enemyId = ""
    private val mode = "Multi Player"
    private val title = "Challenge"
    private var message = ""
    private val date = FieldValue.serverTimestamp()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.send_p1choice)
        myId = sharedPreferences.getString("PLAYERID", "")!!
        myUsername = sharedPreferences.getString("PLAYER_USERNAME", "")!!
        enemyId = intent.getStringExtra("enemyId")!!
        receiveToken = intent.getStringExtra("token")!!
        player2Username = intent.getStringExtra("p2Username")!!
        message = "Tantangan bermain dari $myUsername"
        val profileImg = intent.getStringExtra("profileImg")!!
        Log.d(TAG, "onCreate p2Username: $player2Username ")

        viewModel = ViewModelProvider(this).get(NewBattleViewModel::class.java)
        val btn = listOf<ImageView>(btn_kertas, btn_gunting, btn_batu)

        btn.forEach { buttonChoice ->
            buttonChoice.setOnClickListener {
                val matches = Matches(
                    idP1 = myId,
                    idP2 = enemyId,
                    mode = mode,
                    p1Choice = buttonChoice.contentDescription.toString(),
                    player1Username = myUsername,
                    timestamp = Timestamp.now(),
                    imgUser = profileImg
                )

                PushNotification(
                    NotificationData(
                        myId,
                        title,
                        message,
                        mode
                    ),
                    receiveToken,
                    date
                ).also {
                    viewModel.addDataNotif(it, player2Username)
                }
                viewModel.addMatch(matches, enemyId)

                startActivity(Intent(this, WaitingActivity::class.java))
                finish()
            }
        }

        btn_back.setOnClickListener {
            finish()
        }
    }
}

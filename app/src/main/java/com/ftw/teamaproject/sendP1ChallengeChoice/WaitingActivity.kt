package com.ftw.teamaproject.sendP1ChallengeChoice

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.cc9_single_mainmenu.mainmenu.MainMenuActivityDummy
import com.example.fiturprofil.ui.mainmenu.MainMenuActivity
import com.ftw.teamaproject.R
import com.ftw.teamaproject.multiPlayerMenu.MultiPlayerMenuActivity
import kotlinx.android.synthetic.main.activity_waiting.*

class WaitingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waiting)
        btn_next.setOnClickListener {
            startActivity(Intent(this, MultiPlayerMenuActivity::class.java))
            finish()
        }
        btn_back.setOnClickListener {
            startActivity(Intent(this, MainMenuActivity::class.java))
            finish()
        }
        animationView.apply {
            playAnimation()
        }
    }
}
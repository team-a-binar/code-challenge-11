package com.ftw.teamaproject.sendP1ChallengeChoice

import android.content.ContentValues.TAG
import android.util.Log
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.sendP1ChallengeChoice.model.PushNotification
import com.ftw.teamaproject.sendP1ChallengeChoice.network.RetrofitInstance
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewBattleRepository : DataRepository {

    override fun addMatch(matches: Matches, enemyId: String): Task<DocumentReference> =
        db.collection("matches").add(matches).addOnSuccessListener { doc ->
            Log.d(TAG, "idDoc = ${doc.id}")
            val idMatches = doc.id
            db.collection("matches").document(idMatches).update("idMatch",idMatches)
            db.collection("users").document(enemyId)
                .update("match_new", FieldValue.arrayUnion(idMatches))
        }

    override fun addDataNotif(notificationData: PushNotification, p2Username: String) : Task<DocumentReference> =
        db.collection("notification").document(p2Username).collection("data").add(notificationData)

    override fun sendNotification(notification: PushNotification) =
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = RetrofitInstance.api.postNotification(notification)
                Thread.sleep(3000)
                if (response.isSuccessful) {

                } else {
                    Log.e(TAG, response.errorBody().toString())
                }
            } catch (e: Exception) {
                Log.e(TAG, e.toString())
            }
        }
}
package com.ftw.teamaproject.sendP1ChallengeChoice.network

import com.ftw.teamaproject.sendP1ChallengeChoice.utils.Constants.Companion.CONTENT_TYPE
import com.ftw.teamaproject.sendP1ChallengeChoice.utils.Constants.Companion.SERVER_KEY
import com.ftw.teamaproject.sendP1ChallengeChoice.model.PushNotification
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiServices {
    @Headers("Authorization: key=$SERVER_KEY", "Content-Type:$CONTENT_TYPE")
    @POST("/fcm/send")
    suspend fun postNotification(
        @Body notification: PushNotification
    ): Response <ResponseBody>
}
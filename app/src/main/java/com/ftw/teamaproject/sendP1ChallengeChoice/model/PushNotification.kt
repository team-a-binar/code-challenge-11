package com.ftw.teamaproject.sendP1ChallengeChoice.model


import com.google.firebase.firestore.FieldValue

data class PushNotification (
    var data: NotificationData,
    var to: String,
    var date: FieldValue
)
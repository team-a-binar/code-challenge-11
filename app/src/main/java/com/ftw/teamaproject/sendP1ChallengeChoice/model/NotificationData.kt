package com.ftw.teamaproject.sendP1ChallengeChoice.model


data class NotificationData (
    var p1id:String,
    var title: String,
    var message: String,
    var mode: String
)
package com.ftw.teamaproject.sendP1ChallengeChoice

import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.sendP1ChallengeChoice.model.PushNotification
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import kotlinx.coroutines.Job

interface DataRepository {

    fun addMatch(matches: Matches, enemyId: String): Task<DocumentReference>

    fun addDataNotif(notificationData: PushNotification, p2Username: String) : Task<DocumentReference>

    fun sendNotification(notification: PushNotification) : Job
}
package com.ftw.teamaproject.resultGame.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.model.User
import io.reactivex.disposables.CompositeDisposable

class ResultViewModel(private val repository: ResultRepository) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()

    val matchData = MutableLiveData<Matches>()
    val userData = MutableLiveData<User>()
    val friendsData = MutableLiveData<MutableList<User>>()
    val updateWinnerState = MutableLiveData<String>()
    var userPoin = 0
    val jankenLogicResult = MutableLiveData<String>()
    private val TAG = ResultViewModel::class.java.simpleName

    fun getMatchById(id: String) {
        compositeDisposable.add(
                repository.getMatchById(id)
                        .subscribe({ data ->
                            matchData.postValue(data)
                        }, {
                            Log.d(TAG, "getMatchByIdError: Error $it")
                        })
        )
    }

    fun getUserById(id: String) {
        compositeDisposable.add(repository.getUserById(id).subscribe({ data ->
            userData.postValue(data)
        }, {
            Log.d(TAG, "getUserById: Error ${it.message}")
        }))
    }

    fun updateWinner(match: Matches, result: String, player2Username: String): MutableLiveData<String> {
        repository.updateWinner(match.idMatch, result).addOnSuccessListener {

            if (result == "${match.player1Username} Menang") {
                getUserById(match.idP1)
                userData.observeForever {
                    userPoin = it.point + 5
                    repository.updatePoinUsers(match.idP1, userPoin)
                    Log.d(TAG, "showResult: jumlah poin terkini $userPoin")
                }

            } else if (result == "$player2Username Menang") {
                getUserById(match.idP2)
                userData.observeForever {
                    userPoin = it.point + 5
                    repository.updatePoinUsers(match.idP2, userPoin)
                    Log.d(TAG, "showResult: jumlah poin terkini $userPoin")
                }
            }

            updateWinnerState.postValue("berhasil")
        }.addOnFailureListener {
            updateWinnerState.postValue("gagal")
        }
        return updateWinnerState
    }

    fun jankenLogic(match: Matches, p2Username: String) {
        compositeDisposable.add(
                repository.jankenLogic(match.p1Choice, match.p2Choice, match.player1Username, p2Username).subscribe({ it ->
                    jankenLogicResult.postValue(it)
                },{

                }))
    }

    fun sendNotif(player1Id: String, result: String, senderPlayerId: String, playerUsername: String, enemyName: String) {
        var destToken = ""

        getUserById(player1Id)
        userData.observeForever {
            destToken = it.tokenFcm
            repository.sendNotif(destToken, result, senderPlayerId, playerUsername, enemyName)
        }

    }

    class Factory(private val repository: ResultRepository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            try {
                @Suppress("UNCHECKED_CAST")
                return ResultViewModel(repository) as T
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
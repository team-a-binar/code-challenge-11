package com.ftw.teamaproject.resultGame.ViewModel

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.ftw.resultgame.model.BodyPayloadNotif
import com.ftw.resultgame.model.Data
import com.ftw.resultgame.network.ApiServices
import com.ftw.teamaproject.BuildConfig
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.util.Constants
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.toObject
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


class ResultRepository(private val context: Context) {
    private val TAG = ResultRepository::class.java.simpleName

    private fun servicesNotif(): ApiServices {
        val client = OkHttpClient().newBuilder()
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = if (BuildConfig.DEBUG)
                        HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                })
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL_NOTIF)
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return retrofit.create(ApiServices::class.java)
    }

    private fun pushNotif(bodyPayloadNotif: BodyPayloadNotif) = servicesNotif()
            .pushNotif(Constants.SERVER_KEY, bodyPayloadNotif)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())


    fun getMatchById(id: String): Single<Matches> = Single.create {
        db.collection("matches").document(id).get().addOnSuccessListener { data ->
            if (data != null) {
                val model = data.toObject<Matches>()!!
                it.onSuccess(model)
            }
        }
    }

    fun getUserById(id: String): Single<User> = Single.create {
        db.collection("users").document(id).get().addOnSuccessListener { data ->
            if (data != null) {
                val model = data.toObject<User>()!!
                it.onSuccess(model)
            }
        }
    }

    fun updatePoinUsers(id: String, userPoin: Int) {
        val documentRef = db.collection("users")
                .document(id)
                .update("point", userPoin)
    }

    fun jankenLogic(p1Choice: String,
                    p2Choice: String,
                    p1Username: String,
                    p2Username: String): Single<String> =
            Single.create {
                var result = ""
                if (p1Choice.isNotEmpty() && p2Choice.isNotEmpty()) {
                    Log.d("ResultPresenter", "Isi matchDetails username: p1: $p1Username")
                    when {
                        p1Choice == "batu" && p2Choice == "gunting" -> {
                            it.onSuccess("$p1Username Menang")
                            Log.d("Winner", "$p1Username Menang")
                        }
                        p1Choice == "gunting" && p2Choice == "kertas" -> {
                            it.onSuccess("$p1Username Menang")
                            Log.d("Winner", "$p1Username Menang")
                        }
                        p1Choice == "kertas" && p2Choice == "batu" -> {
                            it.onSuccess("$p1Username Menang")
                            Log.d("Winner", "$p1Username Menang")
                        }
                        p1Choice == p2Choice -> {
                            it.onSuccess("Seri")
                            Log.d("Winner", "Hasil Seri")
                        }
                        else -> {
                            it.onSuccess("$p2Username Menang")
                            Log.d("Winner", "$p2Username Menang")
                        }
                    }
                }
            }

    fun updateWinner(matchesId: String, result: String): Task<Void> {
        val documentRef = db.collection("matches")
                .document(matchesId)
                .update("winner", result)
        Log.d("Repo", "updateWinner: Sukses!")
        return documentRef
    }

    @SuppressLint("CheckResult")
    fun sendNotif(destToken: String, result: String, playerId: String, playerUsername: String, enemyName: String) {
        val dataNotif = BodyPayloadNotif(
                to = destToken,
                date = FieldValue.serverTimestamp(),
                data = Data(
                        idPengirim = playerId,
                        mode = "Multiplayer",
                        title = "Hasil pertandingan",
                        message = "$result melawan $enemyName"
                )
        )
        pushNotif(dataNotif).subscribe({ it ->
            db.collection("notification")
                    .document(playerUsername)
                    .collection("data")
                    .add(dataNotif)
        }, {
            Log.d(TAG, "sendNotif Fail: ${it.message}")
        }
        )
    }
}
package com.ftw.teamaproject.resultGame

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.R
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.multiPlayerMenu.MultiPlayerMenuActivity
import com.ftw.teamaproject.resultGame.ViewModel.ResultViewModel
import kotlinx.android.synthetic.main.activity_result.*
import kotlinx.android.synthetic.main.activity_splash.*
import org.koin.android.ext.android.inject
import java.util.*

class ResultActivity : AppCompatActivity() {
    private val sharedPreferences by lazy {
        getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }
    private val editor by lazy {
        sharedPreferences.edit()
    }
    private lateinit var viewModel: ResultViewModel
    private val factory: ResultViewModel.Factory by inject()
    private var matchesId = "" //dari Intent
    private val TAG = ResultActivity::class.java.simpleName
    private val c: Date = Calendar.getInstance().time
    private var player1Username = ""
    private var player2Username = ""
    private var player1UserId = "" //dari Intent
    private var player2UserId = "" //dari Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        viewModel = ViewModelProvider(this, factory).get(ResultViewModel::class.java)

        matchesId = intent.getStringExtra("match_id")!!
        player2Username = sharedPreferences.getString("PLAYER_USERNAME", "")!!

        var matchDetails: Matches
        viewModel.getMatchById(matchesId)

        viewModel.matchData.observe(this, androidx.lifecycle.Observer {
            matchDetails = it
            player1UserId = matchDetails.idP1
            player2UserId = matchDetails.idP2
            player1Username = matchDetails.player1Username
            btnNextGame.visibility = View.VISIBLE
            play(matchDetails, player2Username)
        })

        btnNextGame.setOnClickListener {
            Intent(this, MultiPlayerMenuActivity::class.java).apply {
                startActivity(this)
                finish()
            }
        }
        val animP1Choose = AnimationUtils.loadAnimation(this, R.anim.blink_anim)
        val animP2Choose = AnimationUtils.loadAnimation(this, R.anim.blink_anim)
        val animResult = AnimationUtils.loadAnimation(this, R.anim.fadein)

        imgP1Choose.startAnimation(animP1Choose)
        imgP2Choose.startAnimation(animP2Choose)
        textViewResult.startAnimation(animResult)
    }

    private fun play(match: Matches, p2Username: String) {
        viewModel.jankenLogic(match, p2Username)

        viewModel.jankenLogicResult.observe(this, androidx.lifecycle.Observer {
            Log.d("ResultActivity", "Isi result: $it")

            when (match.p1Choice) {
                "batu" -> {
                    imgP1Choose.setImageResource(R.drawable.batux)
                }
                "kertas" -> {
                    imgP1Choose.setImageResource(R.drawable.kertasx)
                }
                "gunting" -> {
                    imgP1Choose.setImageResource(R.drawable.guntingx)
                }
            }
            when (match.p2Choice) {
                "batu" -> {
                    imgP2Choose.setImageResource(R.drawable.batux)
                }
                "kertas" -> {
                    imgP2Choose.setImageResource(R.drawable.kertasx)
                }
                "gunting" -> {
                    imgP2Choose.setImageResource(R.drawable.guntingx)
                }
            }
            textViewResult.text = it
            viewModel.updateWinner(match = match, result = it, player2Username = player2Username)
            viewModel.updateWinnerState.observe(this, androidx.lifecycle.Observer { data ->
                if (data == "berhasil") {

                    var enemyName = player2Username //enemyName dinamis, tergantung siapa yg menang. Ex: P1/P2 menang melawan enemyName
                    if (it.contains(player2Username)) {
                        enemyName = player1Username
                    }

                    viewModel.sendNotif(player1UserId, it, player2UserId, player1Username, enemyName)
                    Toast.makeText(this, "Data berhasil dikirim", Toast.LENGTH_SHORT).show()
                } else Toast.makeText(this, "Gagal dikirim", Toast.LENGTH_SHORT).show()
            })
        })
    }
}
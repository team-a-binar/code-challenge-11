package com.ftw.resultgame.network

import com.ftw.resultgame.model.BodyPayloadNotif
import com.ftw.teamaproject.util.Constants.SEND_NOTIF
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiServices {
    @POST(SEND_NOTIF)
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun pushNotif(
        @Header("Authorization") token: String,
        @Body bodyPayloadNotif: BodyPayloadNotif
    ): Single<String>
}
package com.ftw.teamaproject.multiPlayerMenu.newGame

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ftw.teamaproject.R
import com.ftw.teamaproject.model.User
import kotlinx.android.synthetic.main.item_user.view.*

class NewGameAdapter(
    private val data: MutableList<User> = mutableListOf(),
    private var listener: (User) -> Unit = fun(_) {}
) : RecyclerView.Adapter<NewGameAdapter.NewGameViewHolder>() {

    fun setListener(listener: (User) -> Unit) {
        this.listener = listener
    }


    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    fun addData(users: User) {
        Log.d("Add-Users",users.toString())
        this.data.add(users)
        notifyDataSetChanged()
    }

    fun setData(data: MutableList<User>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewGameViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return NewGameViewHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: NewGameViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class NewGameViewHolder(private val v: View) : RecyclerView.ViewHolder(v) {
        fun bind(users: User) {
            Glide.with(v).load(users.image).into(v.ivUser)
            v.tvUsername.text = users.username
            v.itemLayout.setOnClickListener {
                listener.invoke(users)
            }
        }
    }
}
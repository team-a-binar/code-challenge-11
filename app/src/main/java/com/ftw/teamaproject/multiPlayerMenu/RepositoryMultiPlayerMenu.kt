package com.ftw.teamaproject.multiPlayerMenu

import android.content.Context
import android.util.Log
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.model.User
import com.google.firebase.firestore.ktx.toObject
import io.reactivex.Single

class RepositoryMultiPlayerMenu(context: Context) {
    fun getUsers(myId: String): Single<User> = Single.create {
        db.collection("users")
            .document(myId)
            .addSnapshotListener { snapshot, e ->
                val model = snapshot!!.toObject<User>()
                it.onSuccess(model!!)
            }
    }

    fun getFriendsData(friendId: String): Single<User> = Single.create {
        db.collection("users")
            .document(friendId)
            .addSnapshotListener { document, e ->
                val dataFriend = document?.toObject<User>()
                it.onSuccess(dataFriend!!)
                Log.d("REPO", "getMatch:$dataFriend ")
            }
    }


    fun getMatch(matchId: String): Single<Matches> = Single.create {
        db.collection("matches")
            .document(matchId)
            .addSnapshotListener { snapshot, e ->
                val model = snapshot!!.toObject<Matches>()
                Log.d("REPO", "getMatch:$model ")
                it.onSuccess(model!!)
            }
    }
}
package com.ftw.teamaproject.multiPlayerMenu.newGame

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ftw.teamaproject.R
import com.ftw.teamaproject.multiPlayerMenu.MultiPlayerMenuViewModel
import com.ftw.teamaproject.sendP1ChallengeChoice.NewBattleActivity
import kotlinx.android.synthetic.main.fragment_new_game.*
import org.koin.android.ext.android.inject

class NewGame : Fragment() {

    private val sharedPreferences: SharedPreferences by inject()
    private lateinit var adapter: NewGameAdapter
    private val TAG = NewGame::class.java.simpleName
    private val factory: MultiPlayerMenuViewModel.Factory by inject()
    private lateinit var viewModel: MultiPlayerMenuViewModel
    private var myId = ""
    private var userName = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myId = sharedPreferences.getString("PLAYERID", "")!!
        userName = sharedPreferences.getString("PLAYER_USERNAME", "")!!

        viewModel = ViewModelProvider(this, factory).get(MultiPlayerMenuViewModel::class.java)
        adapter = NewGameAdapter()
        rvNewGame.layoutManager = LinearLayoutManager(context)
        rvNewGame.setHasFixedSize(true)
        rvNewGame.adapter = adapter

        viewModel.getFriend(myId)
        viewModel.getMyData(myId)
        showLoading()
        var profilImg = ""

        viewModel.mutableDataImg.observe(viewLifecycleOwner, Observer {
            profilImg = it
        })

        viewModel.mutableDataFriend.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                hideLoading()
                adapter.addData(it)
                Log.d(TAG, "observerData: $it")
            } else {
                hideLoading()
                tvEmptyFriends.visibility = View.VISIBLE
            }

        })

        adapter.setListener { user ->
            val enemyName = user.username
            val enemyId = user.id
            val token = user.tokenFcm
            val imgProfile = profilImg
            Intent(context, NewBattleActivity::class.java).apply {
                putExtra("enemyId", enemyId)
                putExtra("p2Username", enemyName)
                putExtra("token", token)
                putExtra("profileImg", imgProfile)
                startActivity(this)
            }
        }
    }

    fun showLoading() {
        pbNewGame.visibility = View.VISIBLE
    }

    fun hideLoading() {
        pbNewGame.visibility = View.GONE
    }
}

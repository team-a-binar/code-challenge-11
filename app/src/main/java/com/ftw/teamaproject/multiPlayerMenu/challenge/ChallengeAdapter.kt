package com.ftw.teamaproject.multiPlayerMenu.challenge

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.R
import kotlinx.android.synthetic.main.item_user.view.*

class ChallengeAdapter(
    private val data: MutableList<Matches> = mutableListOf(),
    private var listener: (Matches) -> Unit = fun(_) {}
) : RecyclerView.Adapter<ChallengeAdapter.ChallengeViewHolder>() {

    fun setListener(listener: (Matches) -> Unit) {
        this.listener = listener
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }


    fun addData(match: Matches) {
        Log.d("Add-Users",match.toString())
        this.data.add(match)
        notifyDataSetChanged()
    }

    fun setData(data: MutableList<Matches>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChallengeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ChallengeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  data.size
    }

    override fun onBindViewHolder(holder: ChallengeViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class ChallengeViewHolder(private val v: View) : RecyclerView.ViewHolder(v) {
        fun bind(match: Matches) {
            Glide.with(v).load(match.imgUser).into(v.ivUser)
                v.tvUsername.text = match.player1Username
                v.itemLayout.setOnClickListener {
                    listener.invoke(match)
                }
            }
        }
    }
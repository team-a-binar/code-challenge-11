package com.ftw.teamaproject.multiPlayerMenu

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.model.User
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class MultiPlayerMenuViewModel(private val repo: RepositoryMultiPlayerMenu) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val mutableDataFriend = MutableLiveData<User>()
    val mutableDataMatch = MutableLiveData<Matches>()
    val mutableDataImg = MutableLiveData<String>()

    @SuppressLint("CheckResult")
    fun getFriend(myId: String) {
        compositeDisposable.add(
            repo.getUsers(myId)
                .subscribe { data ->
                    getFriends(data)
                })
    }

    fun getFriends(data: User) {
        data.friends.forEach {
            compositeDisposable.add(repo.getFriendsData(it)
                .subscribe { friendData ->
                    mutableDataFriend.value = friendData
                })
        }
    }

    @SuppressLint("CheckResult")
    fun getMatch(myId: String) {
        compositeDisposable.add(
            repo.getUsers(myId)
                .subscribe { data ->
                    getMatchData(data)

                })
    }

    fun getMatchData(data: User) {
        data.match_new.forEach {
            compositeDisposable.add(repo.getMatch(it)
                .filter { match ->
                    match.winner == ""
                }.subscribe { matchData ->
                    mutableDataMatch.value = matchData
                })
        }
    }

    fun getMyData(myId: String) {
        compositeDisposable.add(repo.getUsers(myId)
            .subscribe { it ->
                mutableDataImg.value = it.image
            })
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    class Factory(
        private
        val repository: RepositoryMultiPlayerMenu
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            try {
                @Suppress("UNCHECKED_CAST")
                return MultiPlayerMenuViewModel(repository) as T
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
    }
}
package com.ftw.teamaproject.multiPlayerMenu

interface InterfaceMultiPlayerMenu {


    interface View {
        fun<T> showData(data: T)
        fun clear()
        fun showLoading()
        fun hideLoading()
        fun emptyField()
    }

    interface Presenter {
        fun getData(myId: String)
    }
}
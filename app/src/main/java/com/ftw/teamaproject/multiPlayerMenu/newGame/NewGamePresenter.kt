package com.ftw.teamaproject.multiPlayerMenu.newGame

import android.util.Log
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.multiPlayerMenu.InterfaceMultiPlayerMenu
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class NewGamePresenter : InterfaceMultiPlayerMenu.Presenter {

    var view: InterfaceMultiPlayerMenu.View? = null

    override fun getData(myId: String) {
        view?.showLoading()
        db.collection("users")
            .whereEqualTo("id", myId)
            .addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w("TAG", "Listen failed.", e)
                    return@addSnapshotListener
                }
                val source = if (snapshot != null && snapshot.metadata.hasPendingWrites())
                    "Local"
                else
                    "Server"

                if (snapshot != null && !snapshot.isEmpty) {
                    Log.d("TAG", snapshot.size().toString())
                    val model = snapshot.toObjects<User>().toMutableList()
                    val friends = mutableListOf<User>()

                    GlobalScope.launch(Dispatchers.IO) {
                        model[0].friends.forEach {
                            val frend = getUserById(it)
                            frend?.let { it1 -> friends.add(it1) }
                            Log.d("NewGamePresenter", "getData: $friends")
                        }
                        launch(Dispatchers.Main) {
                            if (friends.size == 0) {
                                view?.emptyField()
                                view?.hideLoading()
                            } else {
                                view?.showData(friends)
                                view?.hideLoading()
                            }
                        }
                    }
                } else {
                    Log.d("TAG", " data: $source null")
                    view?.clear()
                }
            }
    }

    private suspend fun getUserById(id: String): User? {
        var users: User? = null
        db.collection("users")
            .document(id)
            .get()
            .addOnSuccessListener {
                Log.d("getUserById", "Success get User ID : $id")
                val model = it.toObject<User>()
                if (model != null) {
                    users = model
                }
            }
            .addOnFailureListener {
                Log.d("TAG", "Error get User by ID : $id")
            }
            .await()
        Log.d("getUserById", "Success get User Data : $users")
        return users
    }

}
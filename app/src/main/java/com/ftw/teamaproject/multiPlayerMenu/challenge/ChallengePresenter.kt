package com.ftw.teamaproject.multiPlayerMenu.challenge

import android.util.Log
import com.ftw.teamaproject.model.Matches
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.multiPlayerMenu.InterfaceMultiPlayerMenu
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class ChallengePresenter : InterfaceMultiPlayerMenu.Presenter {
    var view: InterfaceMultiPlayerMenu.View? = null
    private val TAG = "GetData"
    override fun getData(myId: String) {
        view?.showLoading()
        db.collection("users")
            .whereEqualTo("id", myId)
            .addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }
                val source = if (snapshot != null && snapshot.metadata.hasPendingWrites())
                    "Local"
                else
                    "Server"

                if (snapshot != null && !snapshot.isEmpty) {
                    Log.d(TAG, snapshot.size().toString())
                    val model = snapshot.toObjects<User>().toMutableList()
                    val matches = mutableListOf<Matches>()

                    GlobalScope.launch(Dispatchers.IO) {
                        model[0].match_new.forEach {
                            val mtch = getMatchById(it)
                            mtch?.let { it1 -> matches.add(it1) }
                        }
//                        Log.d(TAG, "getData Match: ${matches[0].player1Username}")
                        launch(Dispatchers.Main) {
                            view?.showData(matches)
                            view?.hideLoading()
                        }
                    }
                } else {
                    Log.d(TAG, " data: $source null")
                    view?.clear()
                }
            }
    }


    private suspend fun getMatchById(id: String): Matches? {
        var match: Matches? = null
        db.collection("matches")
            .document(id)
            .get()
            .addOnSuccessListener {
                Log.d(TAG, "Success get User ID : $id")
                val model = it.toObject<Matches>()
                if (model != null && model.winner == "") {
                    match = model
                }
            }
            .addOnFailureListener {
                Log.d(TAG, "Error get User by ID : $id")
            }
            .await()
        return match
    }
}
package com.ftw.teamaproject.multiPlayerMenu.challenge

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ftw.teamaproject.R
import com.ftw.teamaproject.multiPlayerMenu.MultiPlayerMenuViewModel
import com.ftw.teamaproject.sendChallengerChoice.SendChallengerChoiceActivity
import kotlinx.android.synthetic.main.fragment_challenge.*
import org.koin.android.ext.android.inject


class Challenge : Fragment() {

    private val sharedPreferences by lazy {
        activity?.getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }
    private val editor by lazy {
        sharedPreferences?.edit()
    }
    private val factory: MultiPlayerMenuViewModel.Factory by inject()
    private lateinit var viewModel: MultiPlayerMenuViewModel
    private lateinit var adapter: ChallengeAdapter
    private val TAG = Challenge::class.java.simpleName

    // ganti ke ID User
    // harusnya get dari sharedPref dari db
    private var myId = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_challenge, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, factory).get(MultiPlayerMenuViewModel::class.java)

        myId = sharedPreferences?.getString("PLAYERID", "")!!
        adapter = ChallengeAdapter()
        rvChallenge.layoutManager = LinearLayoutManager(context)
        rvChallenge.setHasFixedSize(true)
        rvChallenge.adapter = adapter

        viewModel.getMatch(myId)
        showLoading()
        viewModel.mutableDataMatch.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                hideLoading()
                adapter.addData(it)
                Log.d(TAG, "observerData: $it")
            } else {
                hideLoading()
                tvChallengeEmpty.visibility = View.VISIBLE
            }
        })


        adapter.setListener { match ->
            val idP1 = match.idP1
            val idP2 = myId
            val p1Choice = match.p1Choice
            val matchId = match.idMatch

            // ganti ke activity tujuan -- done

            Intent(context, SendChallengerChoiceActivity::class.java).apply {
                putExtra("id_p1", idP1)
                putExtra("id_p2", idP2)
                putExtra("pilihan_1", p1Choice)
                putExtra("match_id", matchId)
                startActivity(this)
            }
        }
    }


    fun showLoading() {
        pbChallenge.visibility = View.VISIBLE
    }

    fun hideLoading() {
        pbChallenge.visibility = View.GONE
    }
}
package com.ftw.teamaproject.sendChallengerChoice

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class SendChallengerChoiceViewModel : ViewModel() {

    private val repository = FirestoreRepoSendChallengerChoice()
    fun sendChoice(matchId: String, result: String ) = viewModelScope.launch {
        repository.sendChallengerChoice(matchId, result)
    }
}
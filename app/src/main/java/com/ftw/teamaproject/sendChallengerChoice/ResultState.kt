package com.ftw.teamaproject.sendChallengerChoice

sealed class ResultState {
    object Loading : ResultState()
    data class Success<T>(val data: T, val msg: String): ResultState()
    data class Error(val throwable: Throwable): ResultState()
}
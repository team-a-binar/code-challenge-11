package com.ftw.teamaproject.sendChallengerChoice

import java.io.Serializable

class Matches(
    var idMatch: String = "",
    var idP1: String = "",
    var idP2: String = "",
    var player1Username: String = "",
    var player2Username: String = "",
    var mode: String = "",
    var p1Choice: String = "",
    var p2Choice: String = "",
    var winner: String = ""
) : Serializable {
    constructor() : this("", "", "", "", "", "", "", "", "")

}
package com.ftw.teamaproject.sendChallengerChoice

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.R
import com.ftw.teamaproject.db
import com.ftw.teamaproject.multiPlayerMenu.EmptyActivity
import com.ftw.teamaproject.resultGame.ResultActivity
import kotlinx.android.synthetic.main.activity_send_challenger_choice.*
import kotlinx.coroutines.tasks.await

class SendChallengerChoiceActivity : AppCompatActivity() {
    private lateinit var matchId: String
    private lateinit var player1choice: String
    private lateinit var viewModel : SendChallengerChoiceViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_challenger_choice)
        viewModel = ViewModelProvider(this).get(SendChallengerChoiceViewModel::class.java)

        matchId = intent.getStringExtra("match_id")!!
        val idP1 = intent.getStringExtra("id_p1")
        val idp2 = intent.getStringExtra("id_p2")


        /*matchId = "0Lu9VS5tr773VRmWmK1z"
        val idP1 = "Puhwg2yymlelfUKLbjF5C5xA8Te2"
        val idp2 = "ebeKrMpOkjWgRCHW6N8BiLgKaeM2"*/

        Log.d("SendChallengerChoiceActivity", "onCreate: Isi match_id $matchId")
        player1choice = intent.getStringExtra("pilihan_1")!!

//        player1choice = "Batu"

        val btn = listOf<ImageView>(iv_batu_send, iv_gunting_send, iv_kertas_send)

        btn.forEach{button ->
            button.setOnClickListener {
                viewModel.sendChoice(matchId, button.contentDescription.toString())
                Intent(this, ResultActivity::class.java).apply {
                    putExtra("pilihan_1", player1choice)
                    putExtra("pilihan_2", button.contentDescription.toString())
                    putExtra("idPlayer1", idP1)
                    putExtra("idPlayer2", idp2)
                    putExtra("match_id", matchId)
                    startActivity(this)
                    finish()
                }
            }
        }

/*        btn.forEach { button ->
            button.setOnClickListener {
                db.collection("matches")
                    .document(matchId)
                    .update("p2Choice", button.contentDescription.toString())
                    .addOnSuccessListener {
                        Log.d("CHALLENGER_SEND", "Pilihan Sukses")
                        Intent(this, ResultActivity::class.java).apply {
                            putExtra("pilihan_1", player1choice)
                            putExtra("pilihan_2", button.contentDescription.toString())
                            putExtra("idPlayer1", idP1)
                            putExtra("idPlayer2", idp2)
                            putExtra("match_id", matchId)
                            startActivity(this)
                            finish()
                        }

                    }.addOnFailureListener {
                        Log.d("CHALLENGER_SEND", "Pilihan Gagal")
                    }

            }
        }*/

    }

}
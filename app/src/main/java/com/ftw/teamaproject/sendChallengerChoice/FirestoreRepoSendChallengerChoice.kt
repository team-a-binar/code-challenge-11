package com.ftw.teamaproject.sendChallengerChoice

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class FirestoreRepoSendChallengerChoice {

    var firestoreDB = FirebaseFirestore.getInstance()

    fun sendChallengerChoice(matchId: String, result: String): Task<Void> {
        var documentReference = firestoreDB.collection("matches")
            .document(matchId)
            .update("p2Choice", result)
        return documentReference
    }


/*aa
    fun sendChallengerChoice(matches: Matches, result): Task<Void> {
        var documentReference = firestoreDB.collection("matches")
            .document(matchId)
            .update("p2Choice", button.contentDescription.toString())
        return collectionReference
    }
*/
}

package com.ftw.teamaproject.util

import android.content.Context

class SharedPref(context: Context) {
    private val pref = context.getSharedPreferences("test", Context.MODE_PRIVATE)
    private val isToken = "isToken"

    var isTokenExpired: Boolean
    set(value) {
        pref.edit()
            .putBoolean(isToken, value)
            .apply()
    }
    get() = pref.getBoolean(isToken, false)
}
package com.ftw.teamaproject.util

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.ftw.teamaproject.history.viewmodel.HistoryRepo
import com.ftw.teamaproject.history.viewmodel.HistoryViewModel
import com.ftw.teamaproject.notifActivity.NotificationRepository
import com.ftw.teamaproject.notifActivity.NotificationViewModel
import com.ftw.teamaproject.resultGame.ViewModel.ResultRepository
import com.ftw.teamaproject.resultGame.ViewModel.ResultViewModel
import com.ftw.teamaproject.sendChallengerChoice.FirestoreRepoSendChallengerChoice
import com.ftw.teamaproject.multiPlayerMenu.MultiPlayerMenuViewModel
import com.ftw.teamaproject.multiPlayerMenu.RepositoryMultiPlayerMenu
import com.ftw.teamaproject.ui.landingPage.LandingPageRepository
import com.ftw.teamaproject.ui.landingPage.LandingPageViewModel
import com.ftw.teamaproject.ui.login.FirebaseLogin
import com.ftw.teamaproject.ui.login.LoginRepository
import com.ftw.teamaproject.ui.login.LoginViewModel
import com.ftw.teamaproject.ui.register.FirebaseRegister
import com.ftw.teamaproject.ui.register.RegisterRepository
import com.ftw.teamaproject.ui.register.RegisterViewModel
import com.ftw.teamaproject.ui.score.ScoreRepository
import com.ftw.teamaproject.ui.score.ScoreViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val myModule = module {

    single { ResultViewModel.Factory(get()) }
    single { NotificationViewModel.Factory(get()) }
    single { ResultRepository(get()) }
    single { NotificationRepository(get()) }
    single { FirestoreRepoSendChallengerChoice() }
    single { MultiPlayerMenuViewModel.Factory(get()) }
    single { RepositoryMultiPlayerMenu(get()) }
    single { LandingPageRepository() }
    single { LandingPageViewModel.Factory(get(), get()) }
    single { RegisterRepository(get()) }
    single { FirebaseRegister() }
    single { RegisterViewModel.Factory(get()) }
    single { LoginRepository(get()) }
    single { FirebaseLogin() }
    single { LoginViewModel.Factory(get()) }
    single { ScoreRepository() }
    single { ScoreViewModel.Factory(get())}
    single { HistoryRepo() }
    single { HistoryViewModel.Factory(get()) }
    single{ getSharedPrefs(androidApplication()) }
    single<SharedPreferences.Editor> { getSharedPrefs(androidApplication()).edit() }

}

fun getSharedPrefs(androidApplication: Application): SharedPreferences{
    return  androidApplication.getSharedPreferences("LOGIN_INFO",  Context.MODE_PRIVATE)
}
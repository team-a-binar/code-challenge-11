package com.ftw.teamaproject.util

import android.content.Context
import android.content.Intent
import com.example.cc9_single_mainmenu.mainmenu.MainMenuActivityDummy
import com.example.fiturprofil.ui.mainmenu.MainMenuActivity
import com.ftw.teamaproject.ui.landingPage.LandingPageActivity
import com.ftw.teamaproject.ui.login.ForgotPasswordActivity
import com.ftw.teamaproject.ui.login.LoginActivity
import com.ftw.teamaproject.ui.register.RegisterActivity

fun Context.startHomeActivity() =
    Intent(this, MainMenuActivity::class.java).also {
        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(it)
    }

fun Context.startRegisterActivity() =
    Intent(this, RegisterActivity::class.java).also {
        startActivity(it)
    }

fun Context.startLoginActivity() =
    Intent(this, LoginActivity::class.java).also {
        startActivity(it)
    }

fun Context.startLandingPageActivity() =
    Intent(this, LandingPageActivity::class.java).also {
        startActivity(it)
    }

fun Context.startForgotPasswordActivity() =
    Intent(this, ForgotPasswordActivity::class.java).also {
        startActivity(it)
    }
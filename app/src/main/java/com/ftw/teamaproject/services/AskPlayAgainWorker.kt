package com.ftw.teamaproject.services

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.fiturprofil.ui.mainmenu.MainMenuActivity
import com.ftw.teamaproject.auth
import io.karn.notify.Notify

class AskPlayAgainWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {

    override fun doWork(): Result {

        return try {
            val username = inputData.getString("USERNAME")
            Thread.sleep(5000)

            if (auth.currentUser != null) {
                if (isStopped) {
                    Log.d(TAG, "doWork: STOPPED")
                } else {
                    Notify
                        .with(applicationContext)
                        .meta {
                            clickIntent = PendingIntent.getActivity(applicationContext,
                                0,
                                Intent(applicationContext, MainMenuActivity::class.java),
                                0)
                        }
                        .content { // this: Payload.Content.Default
                            title = "Ayo main lagi $username"
                            text = "Refreshing dulu yuk 🤗"
                        }
                        .show()
                }
            } else {
                Log.d(TAG, "doWork: User Not Authenticated")
            }
            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }

    companion object {
        private val TAG = "MyWorker"
    }
}
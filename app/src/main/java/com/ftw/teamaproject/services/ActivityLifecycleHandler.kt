package com.ftw.teamaproject.services

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.ftw.teamaproject.auth
import com.ftw.teamaproject.ui.login.LoginActivity

class ActivityLifecycleHandler(private val application: Application) :
    Application.ActivityLifecycleCallbacks {
    private val TAG = "LifecycleCallbacks"
    private var isActive = true
    private val sharedPreferences by lazy {
        application.getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }
    private val request by lazy {
        val username = sharedPreferences.getString("PLAYER_USERNAME", "ERROR USERNAME NOT FOUND")
        val data = Data.Builder()
            .putString("USERNAME", username)
            .build()

        OneTimeWorkRequestBuilder<AskPlayAgainWorker>()
            .setInputData(data)
            .build()
    }

    override fun onActivityPaused(p0: Activity) {
        Log.d(TAG, "onActivityPaused at ${p0.localClassName}")
    }

    override fun onActivityStarted(p0: Activity) {
        Log.d(TAG, "onActivityStarted at ${p0.localClassName} ")
    }

    override fun onActivityDestroyed(p0: Activity) {
        Log.d(TAG, "onActivityDestroyed at ${p0.localClassName}")
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
        Log.d(TAG, "onActivitySaveInstanceState at ${p0.localClassName}")
    }

    override fun onActivityStopped(p0: Activity) {
        Log.d(TAG, "onActivityStopped at ${p0.localClassName}")
        Thread.sleep(1500)
        if (!isActive) {
            WorkManager.getInstance(application).cancelAllWorkByTag("NOTIFICATION TIMER")
            runningNotify()
        } else if (isActive) {
            isActive = false
        }
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
        Log.d(TAG, "onActivityCreated at ${p0.localClassName}")
    }

    override fun onActivityResumed(p0: Activity) {
        isActive = true

        val isTokenActive = sharedPreferences.getBoolean("isTokenActive", true)
        checkTokenExpired(isTokenActive)

        val currentWorkerUID = request.id
        Log.d(TAG, "onActivityResumed at ${p0.localClassName}")
        if (!currentWorkerUID.toString().isNullOrBlank()) {
            WorkManager.getInstance(application).cancelWorkById(currentWorkerUID)
        }
    }

    private fun runningNotify() {
        WorkManager.getInstance(application).enqueueUniqueWork(
            "Coba replace work",
            ExistingWorkPolicy.REPLACE, request
        ) // cara WorkManager dibawah 15 menit
        Log.d(TAG, "runningNotify: Worker dieksekusi")
    }

    private fun checkTokenExpired(isTokenActive: Boolean) {
        if (!isTokenActive) {
            auth.signOut()
            WorkManager.getInstance(application).cancelAllWorkByTag("RequestToken")
            Handler(Looper.getMainLooper()).post(Runnable {
                val intent = Intent(application, LoginActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                application.startActivity(intent)
            })
            sharedPreferences.edit().putBoolean("isTokenActive", true).apply()
        }
    }

}
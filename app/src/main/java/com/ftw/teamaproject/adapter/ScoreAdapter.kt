package com.ftw.teamaproject.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ftw.teamaproject.R
import com.ftw.teamaproject.model.User
import kotlinx.android.synthetic.main.item_score.view.*

class ScoreAdapter(private val context: Context, private val list: MutableList<User> = mutableListOf()
) : RecyclerView.Adapter<ScoreAdapter.ViewHolder>() {

    fun setData(list: MutableList<User>){
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: User){
            Glide.with(itemView).load(user.image).into(itemView.iv_profile)
            itemView.tv_username.text = user.username
            itemView.tv_point.text = user.point.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_score, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }
}
package com.ftw.teamaproject.ui.register

import android.app.AlertDialog
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.R
import com.ftw.teamaproject.databinding.ActivityRegisterBinding
import com.ftw.teamaproject.util.startLoginActivity
import com.google.android.material.snackbar.Snackbar
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_register.*
import org.koin.android.ext.android.inject

class RegisterActivity : AppCompatActivity(), RegisterListener {

    private val TAG = RegisterActivity::class.java.simpleName
    private val dialog: AlertDialog by lazy {
        SpotsDialog.Builder().setContext(this).build()
    }

    private lateinit var viewModels : RegisterViewModel
    private val factory: RegisterViewModel.Factory by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val binding : ActivityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        viewModels = ViewModelProvider(this, factory).get(RegisterViewModel::class.java)
        binding.viewModel = viewModels

        viewModels.authListener = this

        btn_signup.setOnClickListener {
            registerUser()
        }
    }

    private fun registerUser(){
        val username = et_name_signup.text.toString().trim{it <= ' '}
        val email = et_email_signup.text.toString().trim { it <= ' '}
        val password = et_password_signup.text.toString().trim { it <= ' '}

        if (validateForm(username, email, password)){ viewModels.register(username, email, password) }
    }

    fun hideLoading() {
        if (dialog.isShowing) dialog.dismiss()
    }

    fun showLoading() {
        if (!dialog.isShowing) dialog.show()
    }

    fun showError(msg: String) {
        val snackBar = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbar_error_color))
        snackBar.show()
    }

    fun validateForm(username: String, email: String, password: String): Boolean {
        return when {
            TextUtils.isEmpty(username) -> {
                showError("Please enter a name")
                false
            }
            TextUtils.isEmpty(email) -> {
                showError("Please enter an email")
                false
            }
            TextUtils.isEmpty(password) -> {
                showError("Please enter the password")
                false
            } else -> {
                true
            }
        }
    }

    override fun onStarted() {
        showLoading()
    }

    override fun onSuccess() {
        hideLoading()
        startLoginActivity()
    }

    override fun onFailure(msg: String) {
        hideLoading()
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

}
package com.ftw.teamaproject.ui.register

import android.util.Log
import com.ftw.teamaproject.auth
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.util.Constants
import com.google.firebase.firestore.SetOptions
import io.reactivex.Completable

class FirebaseRegister {

    private val TAG = RegisterActivity::class.java.simpleName

    fun register(username: String, email: String, password: String) = Completable.create { emitter ->
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!emitter.isDisposed) {
                    if (it.isSuccessful) {
                        val firebaseUser = it.result!!.user
                        val firebaseId = firebaseUser?.uid
                        val registeredEmail = firebaseUser?.email

                        val user = User(id = firebaseId!!, username = username, email = registeredEmail!!, password = password)

                        db.collection(Constants.USERS)
                            .document(getCUrrentUserId())
                            .set(user, SetOptions.merge())
                            .addOnSuccessListener {
                                auth.signOut()
                            }.addOnFailureListener {e ->
                                Log.e(TAG, "Error", e)
                            }
                        Log.d(TAG, it.result.toString())
                        emitter.onComplete()
                    } else {
                        emitter.onError(it.exception!!)
                    }
                }
            }.addOnFailureListener {
                Log.e(TAG, it.message.toString())
            }
    }

    fun currentUser() = auth.currentUser

    fun getCUrrentUserId(): String {
        var currentUserID = ""
        if (currentUser() != null){
            currentUserID = currentUser()!!.uid
        }
        return currentUserID
    }
}
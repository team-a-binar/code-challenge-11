package com.ftw.teamaproject.ui.landingPage

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.R
import com.ftw.teamaproject.adapter.ViewPagerAdapter
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.ui.splash.SplashActivity
import com.ftw.teamaproject.util.startHomeActivity
import com.ftw.teamaproject.util.startLoginActivity
import kotlinx.android.synthetic.main.activity_landing_page.*
import org.koin.android.ext.android.inject

class LandingPageActivity : AppCompatActivity() {

    private val TAG = SplashActivity::class.java.simpleName
    private lateinit var viewModels: LandingPageViewModel
    private val factory: LandingPageViewModel.Factory by inject()
    private val sharedPreferences by lazy {
        getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing_page)

        viewModels = ViewModelProvider(this, factory).get(LandingPageViewModel::class.java)

        if (sharedPreferences.getBoolean("isLogin", false)){
            checkIfUserIsAuthenticated()
        }

        val position = 0
        val adapter = ViewPagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter
        viewPager.setPageTransformer(true, ZoomOutPageTransformer())
        dots_indicator.setViewPager(viewPager)
    }

    private fun checkIfUserIsAuthenticated() {
        viewModels.checkIfUserIsAUthenticated()
        viewModels.isUserAuthenticatedLiveData.observe(this, Observer<User> {
            if (it.isAuthenticated) {
                startHomeActivity()
                Log.d(TAG, "checkIfUserIsAuthenticated: AUTHENTICATED")
            } else {
                startLoginActivity()
                Log.d(TAG, "checkIfUserIsAuthenticated: NOT AUTHENTICATED")
            }

        })
    }
}
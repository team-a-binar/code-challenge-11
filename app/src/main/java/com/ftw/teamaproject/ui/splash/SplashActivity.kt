package com.ftw.teamaproject.ui.splash

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.R
import com.ftw.teamaproject.auth
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.ui.landingPage.LandingPageActivity
import com.ftw.teamaproject.util.startHomeActivity
import com.ftw.teamaproject.util.startLandingPageActivity
import com.ftw.teamaproject.util.startLoginActivity
import kotlinx.android.synthetic.main.activity_splash.*
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity() {

    private lateinit var mediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val restID = resources.getIdentifier("bright", "raw", packageName)
        mediaPlayer = MediaPlayer.create(this, restID)


        mediaPlayer.setOnPreparedListener {
            mediaPlayer.start()
        }

        mediaPlayer.setOnCompletionListener {
            mediaPlayer.stop()
            startLandingPageActivity()
        }

        val animTitle = AnimationUtils.loadAnimation(this, R.anim.title)
        val animAssets = AnimationUtils.loadAnimation(this, R.anim.bkg)

        imageTitle.startAnimation(animTitle)
        imageBatu.startAnimation(animAssets)
        imageKertas.startAnimation(animAssets)
        imageGunting.startAnimation(animAssets)

    }

    override fun onDestroy() {
        mediaPlayer.release()
        super.onDestroy()
    }

}
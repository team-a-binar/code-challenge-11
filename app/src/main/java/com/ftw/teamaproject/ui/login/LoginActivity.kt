package com.ftw.teamaproject.ui.login

import android.app.AlertDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.ftw.teamaproject.R
import com.ftw.teamaproject.databinding.ActivityLoginBinding
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.util.startForgotPasswordActivity
import com.ftw.teamaproject.util.startHomeActivity
import com.ftw.teamaproject.util.startRegisterActivity
import com.google.android.material.snackbar.Snackbar
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity(), LoginListener {

    private val sharedPreferencesEditor: SharedPreferences.Editor by inject()
    private val TAG = LoginActivity::class.java.simpleName

    private val dialog: AlertDialog by lazy {
        SpotsDialog.Builder().setContext(this).build()
    }

    private lateinit var viewModels: LoginViewModel
    private val factory: LoginViewModel.Factory by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val binding: ActivityLoginBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModels = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        binding.viewModel = viewModels

        viewModels.authListener = this

        tv_toSignUp.setOnClickListener {
            startRegisterActivity()
        }

        btn_signin.setOnClickListener {
            sharedPreferencesEditor.putBoolean("isLogin", true).apply()
            loginUser()
        }

        tv_forget_password.setOnClickListener {
            startForgotPasswordActivity()
        }
    }

    private fun loginUser() {
        val email = et_email_signin.text.toString().trim { it <= ' ' }
        val password = et_password_signin.text.toString().trim { it <= ' ' }

        if (validateForm(email, password)) {
            viewModels.login(email, password)
        }
    }

    private fun startNotifTimer(){
        val notifRequest = PeriodicWorkRequestBuilder<NotifWorker>(15, TimeUnit.MINUTES)
            .setInitialDelay(1, TimeUnit.MINUTES)
            .addTag("NOTIFICATION TIMER")
            .build()
        WorkManager.getInstance(this)
            .enqueueUniquePeriodicWork("haha", ExistingPeriodicWorkPolicy.REPLACE, notifRequest)

        WorkManager.getInstance(this)
            .getWorkInfoByIdLiveData(notifRequest.id)
            .observe(this, Observer {
                when(it.state){
                    WorkInfo.State.SUCCEEDED->{
                        val data = it.outputData
                        val msg = data.getString("response")
                        Log.d(TAG, "WorkManager: $msg")
                    }
                    WorkInfo.State.FAILED->{
                        val data = it.outputData
                        val msg = data.getString("e")
                        Log.d(TAG, "WorkManager: $msg")
                    }
                    else->{}
                }
            })
    }

    private fun runningToken() {

        val requestToken =  OneTimeWorkRequestBuilder<TokenWorker>()
            .addTag("RequestToken")
            .build()

        WorkManager.getInstance(this).enqueueUniqueWork(
            "Coba replace Token",
            ExistingWorkPolicy.REPLACE, requestToken
        )

        Log.d(TAG, "runningToken Worker dieksekusi")

    }

    fun hideLoading() {
        if (dialog.isShowing) dialog.dismiss()
    }

    fun showLoading() {
        if (!dialog.isShowing) dialog.show()
    }

    fun showError(msg: String) {
        val snackBar = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbar_error_color))
        snackBar.show()
    }

    fun validateForm(email: String, password: String): Boolean {
        return when {
            TextUtils.isEmpty(email) -> {
                showError("Please enter an email")
                false
            }
            TextUtils.isEmpty(password) -> {
                showError("Please enter the password")
                false
            }
            else -> {
                true
            }
        }
    }

    fun onReceiveUser(user: User?) {

        val userId = user!!.id
        val userName = user!!.username
        val emailUser = user!!.email

        sharedPreferencesEditor.putString("PLAYERID", userId).apply()
        sharedPreferencesEditor.putString("PLAYER_USERNAME", userName).apply()
        sharedPreferencesEditor.putString("EMAIL", emailUser).apply()

        Log.d(TAG, user.toString())

    }

    override fun onStarted() {
        showLoading()
    }

    override fun onSuccess() {
        hideLoading()
        startHomeActivity()
        sharedPreferencesEditor.putBoolean("isTokenActive", true).apply()
        runningToken()
    }

    override fun onFailure(msg: String) {
        hideLoading()
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
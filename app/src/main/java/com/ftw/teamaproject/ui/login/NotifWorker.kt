package com.ftw.teamaproject.ui.login

import android.content.Context
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import io.karn.notify.Notify

class NotifWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
        return try{
            val outPutData = Data.Builder()
                .putString("response", "Notifikasi Timer Dinyalakan")
                .build()

            Notify
                .with(applicationContext)
                .content {
                    title = "ISTIRAHAT"
                    text = "Anda sudah bermain game selama lebih dari 15 menit"
                }
                .show()

            Result.success(outPutData)

        }catch(e:Exception){

            val outPutData = Data.Builder()
                .putString("e", e.message)
                .build()
            Result.failure(outPutData)

        }
    }
}
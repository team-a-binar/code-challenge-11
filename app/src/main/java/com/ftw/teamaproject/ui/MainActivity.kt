package com.ftw.teamaproject.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ftw.teamaproject.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
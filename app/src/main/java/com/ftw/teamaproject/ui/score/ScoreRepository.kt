package com.ftw.teamaproject.ui.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.util.Constants
import com.google.firebase.firestore.Query
import io.reactivex.Single

class ScoreRepository {
    val mutableData = MutableLiveData<MutableList<User>>()

    fun getListPoint(): LiveData<MutableList<User>> {
        db.collection(Constants.USERS)
            .orderBy("point", Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { document ->
                val listData = mutableListOf<User>()
                for (i in document.documents){
                    val user = i.toObject(User::class.java)
                    listData.add(user!!)
                }
                mutableData.value = listData
            }
        return mutableData
        Log.d("Repository", mutableData.toString())
    }

    fun getListUser(): Single<MutableList<User>> = Single.create {
        db.collection(Constants.USERS)
            .get()
            .addOnSuccessListener { document ->
                val listData = mutableListOf<User>()
                for (i in document.documents){
                    val user = i.toObject(User::class.java)
                    listData.add(user!!)
                }
                it.onSuccess(listData)
            }
    }
}
package com.ftw.teamaproject.ui.landingPage

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.ftw.teamaproject.auth
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.util.Constants
import com.google.firebase.firestore.FirebaseFirestore

class LandingPageRepository {
    private val TAG = LandingPageRepository::class.java.simpleName
    private val firestore = FirebaseFirestore.getInstance()
    private val userCollectionRef = firestore.collection(Constants.USERS)
    private var user: User = User()

    fun checkIfUserAuthenticatedFirebase() : MutableLiveData<User> {
        val isUserAuthenticatedInFirebaseMutableLiveData = MutableLiveData<User>()
        val firebaseUser = auth.currentUser
        if (firebaseUser == null){
            user.isAuthenticated = false
            isUserAuthenticatedInFirebaseMutableLiveData.setValue(user)
        } else if (firebaseUser != null) {
            user.id = firebaseUser.uid
            user.isAuthenticated = true
            isUserAuthenticatedInFirebaseMutableLiveData.setValue(user)
        }
        return isUserAuthenticatedInFirebaseMutableLiveData
    }


    fun addUserToLiveData(id: String) : MutableLiveData<User>{
        val userMutableLiveData: MutableLiveData<User> = MutableLiveData<User>()
        userCollectionRef.document(id).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                    val document = task.result
                    if (document!!.exists()){
                        val user = document.toObject<User>(User::class.java)
                        userMutableLiveData.setValue(user)
                        Log.d(TAG, document.toString())
                    }
                } else {
                    Log.d(TAG, task.exception!!.message)
                }
            }
        return userMutableLiveData
    }
}
package com.ftw.teamaproject.ui.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.model.User
import io.reactivex.disposables.CompositeDisposable

class ScoreViewModel(private val repository: ScoreRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val userData = MutableLiveData<MutableList<User>>()
    private val TAG = ScoreViewModel::class.java.simpleName
    private val repo =
        ScoreRepository()

    fun fetchData() : LiveData<MutableList<User>>{
        val mutableData = MutableLiveData<MutableList<User>>()
        repo.getListPoint().observeForever {
            mutableData.value = it
        }
        return mutableData
    }

    fun getListData(){
        compositeDisposable
            .add(repository.getListUser().subscribe({ data ->
                userData.postValue(data)
            }, {
                Log.d(TAG, "getlistData Error ${it.message}")
            }))
    }

    @Suppress("UNCHEKED_CAST")
    class Factory(private val repository: ScoreRepository) :
        ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ScoreViewModel(repository) as T
        }
    }
}
package com.ftw.teamaproject.ui.landingPage

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.model.User
import java.lang.Exception
import java.lang.RuntimeException

class LandingPageViewModel(application: Application, private val repository: LandingPageRepository) : AndroidViewModel(application) {

    lateinit var isUserAuthenticatedLiveData: LiveData<User>
    lateinit var userLiveData: LiveData<User>

    fun checkIfUserIsAUthenticated(){
        isUserAuthenticatedLiveData = repository.checkIfUserAuthenticatedFirebase()
    }

    fun setUid(id: String){
        userLiveData = repository.addUserToLiveData(id)
    }

    class Factory(private val application: Application, private val repository: LandingPageRepository) : ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            try {
                @Suppress("UNCHECKED_CAST")
                return LandingPageViewModel (application, repository) as T
            } catch (e: Exception){
                throw RuntimeException(e)
            }

        }
    }
}
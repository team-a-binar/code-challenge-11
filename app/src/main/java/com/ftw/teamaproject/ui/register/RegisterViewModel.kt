package com.ftw.teamaproject.ui.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RegisterViewModel(private val repository: RegisterRepository) : ViewModel() {

    var authListener: RegisterListener? = null
    private val disposables = CompositeDisposable()

    val user by lazy { repository.currentUser() }

    fun register(username: String, email: String, password: String){
        if (username.isNullOrEmpty() || email.isNullOrEmpty() || password.isNullOrEmpty()){
            authListener?.onFailure("Please input all values")
            return
        }

        authListener?.onStarted()

        val disposableRegister = repository.register(username, email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                authListener?.onSuccess()
            }, {
                authListener?.onFailure(it.message!!)
            })
        disposables.add(disposableRegister)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    @Suppress("UNCHEKED_CAST")
    class Factory(private val repository: RegisterRepository) :
        ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RegisterViewModel(repository) as T
        }
    }

}
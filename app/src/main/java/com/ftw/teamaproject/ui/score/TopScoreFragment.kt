package com.example.cc9_single_mainmenu.mainmenu

import android.Manifest
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ftw.teamaproject.R
import com.ftw.teamaproject.adapter.ScoreAdapter
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.ui.score.ScoreViewModel
import com.google.android.material.snackbar.Snackbar
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_top_score.*
import org.koin.android.ext.android.inject
import java.io.File
import java.io.FileOutputStream

class TopScoreFragment : Fragment() {

    private lateinit var adapter: ScoreAdapter
    private val TAG = TopScoreFragment::class.java.simpleName

    private val dialog: AlertDialog by lazy {
        SpotsDialog.Builder().setContext(activity).build()
    }

    private val data = mutableListOf<User>()
    private lateinit var viewModels: ScoreViewModel
    private val factory: ScoreViewModel.Factory by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_top_score, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ScoreAdapter(activity?.applicationContext!!)
        viewModels = ViewModelProvider(this, factory).get(ScoreViewModel::class.java)

        rv_list_topscore.setHasFixedSize(true)
        rv_list_topscore.layoutManager = LinearLayoutManager(activity)
        rv_list_topscore.adapter = adapter

        observeData()

        Dexter.withContext(requireActivity())
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) { /* ... */
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) { /* ... */
                }
            }).check()

        btnPrint.setOnClickListener {
            val docsFolder =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            if (!docsFolder.exists()) {
                docsFolder.mkdir()
                Log.i("TAG", "Created a new directory for PDF")
            } else {
                createPDF()

                //process to open file from snackbar
                val file = File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                        .absolutePath + "/TopScore.pdf"
                )
                val uriPdf = FileProvider.getUriForFile(
                    requireActivity(),
                    requireActivity().packageName + ".provider",
                    file
                )
                val snack = Snackbar.make(
                    it,
                    "PDF exported on ${docsFolder.absolutePath}/TopScore.pdf",
                    Snackbar.LENGTH_LONG
                )
                snack.setActionTextColor(Color.RED)
                snack.setAction("OPEN PDF", View.OnClickListener {
                    val target = Intent(Intent.ACTION_VIEW)
                    target.setDataAndType(uriPdf, "application/pdf")
                    target.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

                    val intent = Intent.createChooser(target, "Open File")
                    try {
                        startActivity(intent)
                    } catch (e: ActivityNotFoundException) {
                        Toast.makeText(
                            requireActivity(),
                            "No PDF Reader in your phone",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }).show()

            }
        }
    }

    private fun createPDF() {
        val document = Document(PageSize.A4)
        val table = PdfPTable(
            floatArrayOf(3f, 3f)
        )

        table.defaultCell.horizontalAlignment =
            Element.ALIGN_CENTER
        table.defaultCell.fixedHeight = 50f
        table.totalWidth = PageSize.A4.width
        table.widthPercentage = 100f
        table.defaultCell.verticalAlignment = Element.ALIGN_MIDDLE

        table.addCell("Name")
        table.addCell("Score")

        table.headerRows = 1

        println("Cells = ${table.rows.size}")

        val cells = table.getRow(0).cells
        for (j in cells.indices) {
            cells[j].backgroundColor =
                BaseColor.GRAY
        }

        for (model in data) {
            val name = model.username
            val score = model.point

            table.addCell(name)
            table.addCell(score.toString())
        }

        val pdfname = "TopScore.pdf"
        val docsFolder =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val pdfFile = File(docsFolder.absolutePath, pdfname)
        val output = FileOutputStream(pdfFile)
        PdfWriter.getInstance(document, output)

        document.open()

        val f = Font(
            Font.FontFamily.TIMES_ROMAN,
            30.0f,
            Font.UNDERLINE,
            BaseColor.BLACK
        )

        val g = Font(Font.FontFamily.TIMES_ROMAN, 20.0f, Font.NORMAL, BaseColor.BLUE)

        document.add(Paragraph("Top Score Permainan \n \n", f))
        document.add(table)

        document.close()
        Log.e("Data Table", data.toString())

    }

    private fun observeData() {
        shimmer_view_container.startShimmer()
        viewModels.fetchData().observe(viewLifecycleOwner, Observer { list ->

            val topThreeList = mutableListOf<User>()
            val otherRank = mutableListOf<User>()
            val listRank = mutableListOf<User>()

            list.forEach {
                listRank.add(it)
            }

            Log.d(TAG, list.toString())

            if (list.size > 3) {
                for (i in 0 until list.size) {
                    topThreeList.add(list[i])
                }

            } else {
                for (i in 0..2) {
                    topThreeList.add(list[i])
                }
            }

            if (list[0] != null) {
                tv_username_1.text = list[0].username
                tv_username_1_point.text = list[0].point.toString()
            }
            if (list[1] != null) {
                tv_username_2.text = list[1].username
                tv_username_2_point.text = list[1].point.toString()
            }

            if (list[2] != null) {
                tv_username_3.text = list[2].username
                tv_username_3_point.text = list[2].point.toString()
            }

            for (i in 3 until list.size) {
                otherRank.add(list[i])
            }
            shimmer_view_container.stopShimmer()
            shimmer_view_container.visibility = View.GONE
            adapter.setData(otherRank)

            this.data.addAll(listRank)

        })
    }

}

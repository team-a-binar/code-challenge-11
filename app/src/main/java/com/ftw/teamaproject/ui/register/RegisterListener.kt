package com.ftw.teamaproject.ui.register

interface RegisterListener {
    fun onStarted()
    fun onSuccess()
    fun onFailure(msg: String)
}
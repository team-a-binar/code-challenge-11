package com.ftw.teamaproject.ui.login

import android.util.Log
import com.ftw.teamaproject.auth
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.User
import com.ftw.teamaproject.util.Constants
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Completable

class FirebaseLogin {

    private val TAG = LoginActivity::class.java.simpleName
    private val activity = LoginActivity()

    fun login(email: String, password: String) = Completable.create { emitter ->
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!emitter.isDisposed){
                    if (it.isSuccessful){
                        db.collection(Constants.USERS)
                            .document(getCurrentUserId())
                            .get()
                            .addOnSuccessListener { document ->
                                val loggedInUser = document.toObject(User::class.java)
                                //signInSuccess(loggedInUser!!)
                                activity.onReceiveUser(loggedInUser)
                                updateFcmToken(loggedInUser!!)
                            }.addOnFailureListener { e ->
                                Log.e(TAG, e.message.toString())
                            }
                        emitter.onComplete()
                     } else {
                        emitter.onError(it.exception!!)
                        Log.e(TAG, it.exception?.message)
                     }
                }
            }
    }

    fun resetPassword(email: String) = Completable.create { emitter ->
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener {
                if (!emitter.isDisposed){
                    if (it.isSuccessful){
                        emitter.onComplete()
                    } else {
                        emitter.onError(it.exception!!)
                    }
                }
            }.addOnFailureListener {
                Log.e(TAG, it.message.toString())
            }
    }

    private fun updateFcmToken(user: User) {
        val userId = user.id
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@addOnCompleteListener
                }

                // Get new Instance ID token
                val tokenFcm = task.result?.token

                var ref = db.collection("users")
                    .document(userId)
                    .update("tokenFcm", tokenFcm)
        }
    }

    fun currentUser() = auth.currentUser

    fun getCurrentUserId(): String {
        var currentUserID = ""
        if (currentUser() != null){
            currentUserID = currentUser()!!.uid
        }
        return currentUserID
    }
}
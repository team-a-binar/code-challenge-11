package com.ftw.teamaproject.ui.login

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.ftw.teamaproject.R
import com.ftw.teamaproject.databinding.ActivityForgotPasswordBinding
import com.ftw.teamaproject.util.startLoginActivity
import com.google.android.material.snackbar.Snackbar
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.koin.android.ext.android.inject

class ForgotPasswordActivity : AppCompatActivity(), LoginListener {

    private val TAG = ForgotPasswordActivity::class.java.simpleName

    private val dialog: AlertDialog by lazy {
        SpotsDialog.Builder().setContext(this).build()
    }

    private lateinit var viewModels: LoginViewModel
    private val factory: LoginViewModel.Factory by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        val binding: ActivityForgotPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)

        viewModels = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        binding.viewModelForgotPassword = viewModels

        viewModels.authListener = this

        btn_reset.setOnClickListener {
            resetPassword()
        }
    }

    private fun resetPassword(){
        val email = et_email_reset.text.toString().trim{ it <= ' '}

        if (validateForm(email)){
            viewModels.resetPassword(email)
        }
    }

    fun hideLoading(){
        if (dialog.isShowing) dialog.dismiss()
    }

    fun showLoading(){
        if (!dialog.isShowing) dialog.show()
    }

    fun showError(msg: String){
        val snackBar = Snackbar.make(findViewById(R.id.content), msg, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbar_error_color))
        snackBar.show()
    }

    fun validateForm(email: String) : Boolean {
        return when {
            TextUtils.isEmpty(email) -> {
                showError("Please enter a valid email")
                false
            }
            else -> {
                true
            }
        }
    }

    override fun onStarted() {
        showLoading()
    }

    override fun onSuccess() {
        hideLoading()
        startLoginActivity()
    }

    override fun onFailure(msg: String) {
        hideLoading()
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
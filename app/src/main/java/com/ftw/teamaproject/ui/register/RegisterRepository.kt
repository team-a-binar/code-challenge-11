package com.ftw.teamaproject.ui.register

class RegisterRepository(private val firebase: FirebaseRegister) {
    fun register(username: String, email: String, password: String) = firebase.register( username, email, password)
    fun currentUser() = firebase.currentUser()
}
package com.ftw.teamaproject.ui.login

interface LoginListener {
    fun onStarted()
    fun onSuccess()
    fun onFailure(msg: String)
}
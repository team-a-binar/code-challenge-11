package com.ftw.teamaproject.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class LoginViewModel(private val repository: LoginRepository) : ViewModel() {

    var authListener: LoginListener? = null
    private val disposables = CompositeDisposable()
    val loginState = MutableLiveData<String>()

    val user by lazy { repository.currentUser() }

    fun login(email: String, password: String){
        if (email.isNullOrEmpty() || password.isNullOrEmpty()){
            authListener?.onFailure("invalid email or password")
            return
        }
        authListener?.onStarted()

        val disposableLogin = repository.login(email!!, password!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                authListener?.onSuccess()
            }, {
                authListener?.onFailure("Email dan Password tidak terdaftar")
            })
        disposables.add(disposableLogin)
    }

    fun resetPassword(email: String){
        if (email.isNullOrEmpty()){
            authListener?.onFailure("invalid email")
            return
        }

        authListener?.onStarted()

        val disposedReset = repository.resetPassword(email!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                authListener?.onSuccess()
            }, {
                authListener?.onFailure("Email tidak valid")
            })
        disposables.add(disposedReset)
    }

    fun loginTest(email: String, password: String){
        disposables.add(
            repository.loginTest(email, password)
                .subscribe(Consumer {
                    loginState.postValue(it)
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    @Suppress("UNCHEKED_CAST")
    class Factory(private val repository: LoginRepository) :
        ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginViewModel(repository) as T
        }
    }
}
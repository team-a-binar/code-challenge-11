package com.ftw.teamaproject.ui.login

import io.reactivex.Single

class LoginRepository(private val firebase: FirebaseLogin) {
    fun login(email: String, password: String) = firebase.login(email, password)
    fun resetPassword(email: String) = firebase.resetPassword(email)
    fun currentUser() = firebase.currentUser()
    fun loginTest(email: String, password: String): Single<String> = Single.create{
        if (email.isNullOrEmpty() && password.isNullOrEmpty()){
            it.onSuccess("Email dan passwordnya ada")
        } else {
            it.onSuccess("Email dan passwordnya kosong")
        }
    }
}
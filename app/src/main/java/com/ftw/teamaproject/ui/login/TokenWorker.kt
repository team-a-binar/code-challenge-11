package com.ftw.teamaproject.ui.login

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters


class TokenWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    private val sharedPreferences by lazy {
        context.getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }

    override fun doWork(): Result {

        return try {
            Thread.sleep(3600000)
            sharedPreferences.edit().putBoolean("isTokenActive", false).apply()
            val isTokenActive = sharedPreferences.getBoolean("isTokenActive", true)

            Log.d("TokenWorker", "doWork: $isTokenActive")
            Result.success()

        } catch (e: Exception) {
            Log.d("TokenWorkerError", "doWork: $e")
            Result.failure()
        }
    }

}

package com.ftw.teamaproject.notifActivity

import android.content.Context
import com.ftw.teamaproject.db
import com.google.firebase.firestore.Query
import io.reactivex.Single

class NotificationRepository(private val context: Context) {

    fun getNotificationData(playerUsername: String): Single<Query> = Single.create {
        val data = db.collection("notification")
                .document(playerUsername)
                .collection("data")
                .orderBy("date", Query.Direction.DESCENDING)

        it.onSuccess(data)

    }
}
package com.ftw.teamaproject.notifActivity

import com.google.firebase.firestore.Query

sealed class ResultStateNotification {
    object Loading : ResultStateNotification()
    data class Success<Query>(val data: Query, val msg: String) : ResultStateNotification()
    data class Error(val throwable: Throwable) : ResultStateNotification()
}

package com.example.cc9_single_mainmenu.mainmenu

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.ftw.teamaproject.R
import com.ftw.teamaproject.db
import com.ftw.teamaproject.model.NotificationModel
import com.ftw.teamaproject.notifActivity.NotificationAdapter
import com.ftw.teamaproject.notifActivity.NotificationViewModel
import com.ftw.teamaproject.notifActivity.ResultStateNotification
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_notification.*
import org.koin.android.ext.android.inject

class NotificationActivity : AppCompatActivity() {
    private val sharedPreferences by lazy {
        getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
    }
    private val editor by lazy {
        sharedPreferences.edit()
    }
    private lateinit var viewModel: NotificationViewModel
    private val factory: NotificationViewModel.Factory by inject()
    private val TAG = NotificationActivity::class.java.simpleName
    private var adapter: NotificationAdapter? = null
    private var playerUsername = ""
    private var playerId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        playerUsername = sharedPreferences.getString("PLAYER_USERNAME", "")!!
        playerId = sharedPreferences.getString("PLAYERID", "")!!
        viewModel = ViewModelProvider(this, factory).get(NotificationViewModel::class.java)
        btnBackNotif.setOnClickListener {
            finish()
        }
        initRecyclerView()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
        adapter?.stopListening()
    }

    private fun initRecyclerView() {
        rvNotification.layoutManager = LinearLayoutManager(this)
        rvNotification.setHasFixedSize(true)

        viewModel.getNotificationData(playerUsername)
        viewModel.dataNotification.observe(this, Observer {

            val options: FirestoreRecyclerOptions<NotificationModel> =
                FirestoreRecyclerOptions.Builder<NotificationModel>()
                    .setQuery((it), NotificationModel::class.java)
                    .build()

            adapter = NotificationAdapter(options = options)
            rvNotification.adapter = adapter
            adapter?.startListening()
            adapter?.setOnClickListener(object : NotificationAdapter.OnClickListener {
                override fun onClickAdd(notification: NotificationModel, position: Int) {
                    db.collection("users")
                        .document(playerId)
                        .update(
                            "friends",
                            FieldValue.arrayUnion(notification.data.get("idPengirim"))
                        )
                        .addOnSuccessListener {
                            Toast.makeText(
                                this@NotificationActivity,
                                "Sukses menambahkan teman",
                                Toast.LENGTH_SHORT
                            ).show()
                            adapter?.deleteData(position)
                        }
                }

                override fun onClickDeleteDirect(notification: NotificationModel, position: Int) {
                    adapter?.deleteData(position)
                }
            })
        })
        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                adapter?.deleteData(viewHolder.adapterPosition)
            }
        }).attachToRecyclerView(rvNotification)
    }

    private fun getResult(resultStateNotification: ResultStateNotification): Query? {
        if (resultStateNotification is ResultStateNotification.Success<*>) {
            val query = resultStateNotification.data as Query
            return query
        }
        return null
    }
}
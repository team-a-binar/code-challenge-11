package com.ftw.teamaproject.notifActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.ftw.teamaproject.R
import com.ftw.teamaproject.model.NotificationModel
import kotlinx.android.synthetic.main.item_notification.view.*
import java.text.SimpleDateFormat
import java.util.*


class NotificationAdapter(
        private val options: FirestoreRecyclerOptions<NotificationModel>
) : FirestoreRecyclerAdapter<NotificationModel, NotificationAdapter.NotificationViewHolder>(options!!) {

    private lateinit var listener: OnClickListener

    override fun updateOptions(options: FirestoreRecyclerOptions<NotificationModel>) {
        super.updateOptions(options)
    }

    fun setOnClickListener(listener: OnClickListener) {
        this.listener = listener
    }

    fun deleteData(position: Int) {
        snapshots.getSnapshot(position).reference.delete()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return NotificationViewHolder(view)
    }


    override fun onBindViewHolder(
            holder: NotificationViewHolder,
            position: Int,
            model: NotificationModel
    ) {
        holder.bind(model, position)
    }

    inner class NotificationViewHolder(private val v: View) : RecyclerView.ViewHolder(v) {
        fun bind(notification: NotificationModel, position: Int) {

            fun Date?.parseDate(): String {
                val inputFormat = notification.date
                val date = inputFormat?.toDate()
                val outputFormat = SimpleDateFormat("dd/MM/yyyy", Locale("ID"))
                return outputFormat.format(date)
            }

            if (notification.data.get("mode") == "Friendlist") {
                v.layoutFriendConfirm.visibility = View.VISIBLE
            } else v.layoutFriendConfirm.visibility = View.GONE

            v.tvTitle.text = notification.data.get("title")
            v.tvMode.text = notification.data.get("mode")
            v.tvDate.text = notification.date?.toDate().parseDate()

            listener.let {
                v.btnConfirm.setOnClickListener {
                    listener.onClickAdd(notification, position)
                }
                v.btnDelete.setOnClickListener {
                    listener.onClickDeleteDirect(notification, position)
                }
            }
        }

    }

    interface OnClickListener {
        fun onClickAdd(notification: NotificationModel, position: Int)
        fun onClickDeleteDirect(notification: NotificationModel, position: Int)
    }
}
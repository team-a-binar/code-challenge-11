package com.ftw.teamaproject.notifActivity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.Query
import io.reactivex.disposables.CompositeDisposable

class NotificationViewModel(private val repository: NotificationRepository) : ViewModel() {
    val dataNotification = MutableLiveData<Query>()
    private val compositeDisposable = CompositeDisposable()

    fun getNotificationData(playerUsername: String) {
        compositeDisposable.add(repository.getNotificationData(playerUsername).subscribe({ it ->
            dataNotification.postValue(it)
        }, {

        }))
    }

    class Factory(private val repository: NotificationRepository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            try {
                @Suppress("UNCHECKED_CAST")
                return NotificationViewModel(repository) as T
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}